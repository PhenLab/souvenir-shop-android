package com.example.themaninthemiddl.souvenirshop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.FavoriteShopAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.favoriteProductAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritedShopFragment extends Fragment {


    private RecyclerView recyclerView;
    private FavoriteShopAdapter favoritedShopAdapter;
    private List<Shops> favoritedShopList;

    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getFavoritedShopListCallbackCode = 1;
    private String getFavoritedShopListUrl = "/shop/favorite/by-user-id";

    private ImageView addProduct, searchFilter;
    private ImageConverter imageConverter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_favorited_shop, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.favorited_shop_recycler_view);

        favoritedShopList = new ArrayList<>();

        favoritedShopAdapter = new FavoriteShopAdapter(getActivity(), favoritedShopList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(favoritedShopAdapter);

        authentication = new Authentication(getActivity());

        param.put("user_id",authentication.getUserPreference().getString("id",null));
        param.put("type",getString(R.string.shop_type));

        initVolleyCallback();
        imageConverter = new ImageConverter(getActivity());
        volleyService = new VolleyService(resultCallback,getActivity());
        volleyService.makeApiRequest(getFavoritedShopListCallbackCode, getFavoritedShopListUrl,param);

        return view;

    }


    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {


                Log.i("fav_sho_response",""+response);

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getFavoritedShopListCallbackCode) {
                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Shops shop = new Shops(jsonObject.getString("id"), jsonObject.getString("image"),"+" +jsonObject.getString("product_numbers"), jsonObject.getString("name"));

                            favoritedShopList.add(shop);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    favoritedShopAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Toast.makeText(getActivity(), "get_fav_sho_error "+ error, Toast.LENGTH_SHORT).show();

            }
        };
    }


}
