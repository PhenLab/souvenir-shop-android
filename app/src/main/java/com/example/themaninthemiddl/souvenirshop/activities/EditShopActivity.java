package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class EditShopActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int GET_IMAGE_FROM_CAMERA_CODE = 1;
    private static final int GET_IMAGE_FROM_GALLERY_CODE = 2;
    private static final int PLACE_PICKER_REQUEST = 3;



    private Uri shopImgUri;
    private InputStream imgInputStream;
    private Bitmap shopImgBitmap;

    private Toolbar toolbar;
    private EditText edtShopName, edtEmail, edtPhone, edtWebsite, edtLat, edtLong,edtDescription;
    private ImageView profile;
    private AppCompatButton btnPickLocation, updateShop;

    //image
    private ImageConverter imageConverter;

    private String imageBase64String;

    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int updateShopCallbackCode = 1;
    private String updateShopUrl = "/shop/update";
    private int getShopByIDCallbackCode = 2;
    private String getShopByIDUrl = "/get-shop-by-id";

    String shop_id = null;
    Double lat, Long;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shop);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        imageConverter = new ImageConverter(this);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.edit_shop));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edtShopName = (EditText)findViewById(R.id.shop_name);
        edtPhone = (EditText)findViewById(R.id.phone);
        edtEmail = (EditText)findViewById(R.id.email);
        edtWebsite = (EditText)findViewById(R.id.website);
        edtLat = (EditText)findViewById(R.id.lat);
        edtLong = (EditText)findViewById(R.id.Long);
        edtDescription = (EditText)findViewById(R.id.description);
        btnPickLocation = (AppCompatButton) findViewById(R.id.btn_pick_location);

        btnPickLocation.setOnClickListener(this);
        profile = (ImageView) findViewById(R.id.profile);
        updateShop = (AppCompatButton) findViewById(R.id.btn_update_shop);

        profile.setOnClickListener(this);
        updateShop.setOnClickListener(this);

        authentication = new Authentication(this);

        initializeVolleyCallback();
        volleyService = new VolleyService(resultCallback, getApplication());

        shop_id = getIntent().getStringExtra("shop_id");
        param.put("shop_id",shop_id);

        loadShop();


    }

    private void loadShop() {

        swipeRefreshLayout.setRefreshing(true);
        volleyService.makeApiRequest(getShopByIDCallbackCode,getShopByIDUrl,param);

    }

    private void initializeVolleyCallback() {
        resultCallback =new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                if (callBackCode == updateShopCallbackCode) {
                    Intent intent = new Intent(getApplication(),ShopDetailActivity.class);
                    intent.putExtra("SHOP_ID",shop_id);
                    startActivity(intent);
                }
                else {
                    Log.i("get_shop",response+"");
                    JSONObject jsonObject;

                    try {
                        jsonObject = response.getJSONObject("data");
                        edtPhone.setText(jsonObject.getString("phone"));
                        edtEmail.setText(jsonObject.getString("email"));
                        edtWebsite.setText(jsonObject.getString("website"));
                        edtShopName.setText(jsonObject.getString("name"));
                        edtLat.setText(jsonObject.getString("lat"));
                        edtLong.setText(jsonObject.getString("lng"));
                        edtDescription.setText(jsonObject.getString("description"));

                        imageBase64String = jsonObject.getString("image");
                        if (!(jsonObject.getString("image")).equals("null")){
                            profile.setImageBitmap(imageConverter.convertFromBase64ToBitmap(jsonObject.getString("image")));
                        }
                        swipeRefreshLayout.setRefreshing(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Toast.makeText(EditShopActivity.this, "create error "+ error, Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.profile:

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(EditShopActivity.this);
                builderSingle.setTitle(getResources().getString(R.string.choose_option));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditShopActivity.this, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add(getResources().getString(R.string.camera));
                arrayAdapter.add(getResources().getString(R.string.gallery));


                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        if (which == 0){
                            launchCameraClick();
                        }
                        else {
                            openGalleryClick();
                        }
                    }
                });
                builderSingle.show();

                break;

            case R.id.btn_update_shop:
                param.clear();
                swipeRefreshLayout.setRefreshing(true);
                param.put("name", edtShopName.getText().toString());
                param.put("phone", edtPhone.getText().toString());
                param.put("email", edtEmail.getText().toString());
                param.put("website", edtWebsite.getText().toString());
                param.put("image",imageBase64String);
                param.put("user_id",authentication.getUserPreference().getString("id",null));
                param.put("shop_id",shop_id);
                param.put("lat",edtLat.getText().toString());
                param.put("Long",edtLong.getText().toString());
                param.put("description",edtDescription.getText().toString());

                volleyService.makeApiRequest(updateShopCallbackCode,updateShopUrl,param);
                break;


            case R.id.btn_pick_location:

                Log.i("pick_loc","true");
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_IMAGE_FROM_GALLERY_CODE && resultCode == RESULT_OK){

            shopImgUri = data.getData();
            shopImgBitmap = imageConverter.loadFitImage(shopImgUri,300,300);

            profile.setImageBitmap(shopImgBitmap);
            //convert from bitmap to base64 string
            imageBase64String = imageConverter.convertFromBitmapToBase64(shopImgBitmap);

            Log.i("base64",","+imageBase64String+",");

        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                edtLat.setText(String.valueOf(place.getLatLng().latitude));
                edtLong.setText(String.valueOf(place.getLatLng().longitude));

                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
        else if (requestCode == GET_IMAGE_FROM_CAMERA_CODE && resultCode == RESULT_OK){

            shopImgUri = (Uri) data.getExtras().get("data");
            shopImgBitmap = imageConverter.loadFitImage(shopImgUri,300,300);
            profile.setImageBitmap(shopImgBitmap);
            imageBase64String = imageConverter.convertFromBitmapToBase64(shopImgBitmap);


        }
    }

    //camera
    public void launchCameraClick(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,GET_IMAGE_FROM_CAMERA_CODE);

    }

    //gallery
    public void openGalleryClick(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,GET_IMAGE_FROM_GALLERY_CODE);

    }

}
