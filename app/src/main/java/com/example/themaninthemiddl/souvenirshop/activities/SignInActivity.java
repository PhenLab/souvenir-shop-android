package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{

    Toolbar toolbar;
    private  AppCompatButton btnSingIn, btnSingUp;

    private EditText phone, password;
    SwipeRefreshLayout swipeRefreshLayout;

    //data in share preference
    private String userData = "user_data";
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int signInCallbackCode = 1;
    private String signInUrl = "/login", item_id = null, item_type ;
    boolean is_favorite = false;
    private String createFavoritedUrl = "/favorite/add";
    private int createFavoritedCallbackCode = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.sign_in));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                if (is_favorite == true){
                    if (item_type == getString(R.string.shop_type)){
                        Intent intent = new Intent(getApplication(), ShopDetailActivity.class);
                        intent.putExtra("SHOP_ID", item_id);
                        startActivity(intent);
                        finish();


                    }else {
                        Intent intent = new Intent(getApplication(), ProductDetailActivity.class);
                        intent.putExtra("product_id", item_id);
                        startActivity(intent);
                        finish();
                    }
                }else {
                    startActivity(new Intent(getApplication(), MainActivity.class));
                }
            }
        });

        btnSingIn = (AppCompatButton)findViewById(R.id.btn_sign_in);
        btnSingIn.setOnClickListener(this);

        btnSingUp = (AppCompatButton)findViewById(R.id.btn_sign_up);
        btnSingUp.setOnClickListener(this);

        phone = (EditText)findViewById(R.id.phone);
        password = (EditText)findViewById(R.id.password);

        is_favorite = getIntent().getBooleanExtra("is_favorite",false);
        item_type = getIntent().getStringExtra("type");
        item_id = getIntent().getStringExtra("item_id");

        //sever
        initVolleyCallback();
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        volleyService = new VolleyService(resultCallback,getApplication());

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                if (callBackCode == signInCallbackCode) {

                    SharedPreferences pref = getApplicationContext().getSharedPreferences(userData, MODE_PRIVATE); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();

                    try {
                        editor.putString("id", response.getString("id"));
                        editor.putString("first_name", response.getString("first_name"));
                        editor.putString("last_name", response.getString("last_name"));
                        editor.putString("user_name", response.getString("user_name"));
                        editor.putString("phone", response.getString("phone"));
                        editor.putString("email", response.getString("email"));
                        editor.putString("image", response.getString("image"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    swipeRefreshLayout.setRefreshing(false);

                    editor.commit();

                    Log.i("error", response + "");
                    Log.i("user_data_save", pref.getString("id", "null") + ","
                            + pref.getString("first_name", "null") + "" + pref.getString("last_name", "null") + ","
                            + pref.getString("user_name", "null") + "" + pref.getString("phone", "null"));

                    if (is_favorite == true) {

                        param.put("user_id", pref.getString("id", "null"));
                        param.put("item_id", item_id);
                        param.put("type", item_type);
                        volleyService.makeApiRequest(createFavoritedCallbackCode, createFavoritedUrl, param);
                    } else {
                        finish();
                        startActivity(new Intent(getApplication(), MainActivity.class));
                    }

                }
                else {
                    if (item_type.equals(getString(R.string.shop_type))){
                        Intent intent = new Intent(getApplication(), ShopDetailActivity.class);
                        intent.putExtra("SHOP_ID", item_id);
                        intent.putExtra(getString(R.string.login_to_get_permision), true);

                        startActivity(intent);
                        finish();


                    }else {
                        Intent intent = new Intent(getApplication(), ProductDetailActivity.class);
                        intent.putExtra("product_id", item_id);
                        intent.putExtra(getString(R.string.login_to_get_permision), "true");

                        startActivity(intent);
                        finish();
                    }
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(SignInActivity.this, "log in error"+ error, Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_sign_in:

                swipeRefreshLayout.setRefreshing(true);
                param.put("email",phone.getText().toString());
                param.put("password",password.getText().toString());

                volleyService.makeApiRequest(signInCallbackCode, signInUrl,param);
                break;

            case R.id.btn_sign_up:
                Intent intent = new Intent(getApplication(),SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                if (is_favorite == true) {
                    intent.putExtra("is_favorite", true);
                    intent.putExtra("type", item_type);
                    intent.putExtra("item_id", item_id);

                }
                startActivity(new Intent(getApplication(), SignUpActivity.class));
                break;
        }
    }
}
