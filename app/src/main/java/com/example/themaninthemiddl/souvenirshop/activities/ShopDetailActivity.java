package com.example.themaninthemiddl.souvenirshop.activities;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.myShopAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.LocationService;
import com.example.themaninthemiddl.souvenirshop.data.Shops;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShopDetailActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, RoutingListener {

    private Toolbar toolbar;
    private GoogleMap google_map;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static GoogleApiClient mGoogleApiClient;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    //image
    private ImageConverter imageConverter;

    private String imageBase64String;

    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String, String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getShopByIDCallbackCode = 1;
    private String getShopByIDUrl = "/get-shop-by-id";
    private int deleteShopCallbackCode = 2;
    private String deleteShopUrl = "/shop/delete";

    private String createFavoritedShopUrl = "/favorite/add";
    private int createFavoritedShopCallbackCode = 3;

    private String deleteFavoritedShopUrl = "/favorite/delete";
    private int deleteFavoritedShopCallbackCode = 4;

    private String getNearbyShopUrl = "/shop/nearby";
    private int getNearbyShopCallbackCode = 5;
    private CircleImageView shopImage;

    String shop_id = null;
    String shopName = null;

    RelativeLayout products, messages;
    ImageView editShop, imvDeleteShop, imvFavorite;
    private TextView address, phone, email, website, description, shop_name;

    Double lat = null, Long = null;
    boolean is_favorite = false;
    ShopDetailActivity onMapReady = null;
    LatLng shopLocationLatLng = null, currentLocation = null;
    private List<Polyline> polylines;
    private static final int[] COLORS = new int[]{R.color.black};
    SupportMapFragment mapFragment;
    SwipeRefreshLayout swipeRefreshLayout;
    int step = 1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    LocationService locationService;
    ArrayList<Shops> nearByShopList;
    Marker userMarker , shopMarker;
    private myShopAdapter myShopAdapter;
    RecyclerView nearbyShopRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getSupportActionBar().setTitle(getResources().getString(R.string.shop_detail));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

                if (getIntent().getBooleanExtra(getString(R.string.login_to_get_permision), false) == true) {
                    startActivity(new Intent(getApplication(), MainActivity.class));
                }
            }
        });

        locationService = new LocationService(getApplication());

        Log.i("test_my_Loc", "" + locationService.getCurrentLocation());
        initVolleyCallback();
        volleyService = new VolleyService(resultCallback, this);
        shop_id = getIntent().getStringExtra("SHOP_ID");
        Toast.makeText(this, "shop Id" + shop_id, Toast.LENGTH_SHORT).show();

        param.put("shop_id", shop_id);

        address = (TextView) findViewById(R.id.address);
        phone = (TextView) findViewById(R.id.phone);
        email = (TextView) findViewById(R.id.email);
        website = (TextView) findViewById(R.id.website);
        description = (TextView) findViewById(R.id.description);

        shop_name = (TextView) findViewById(R.id.shop_name);
        shopImage = (CircleImageView) findViewById(R.id.image);

        products = (RelativeLayout) findViewById(R.id.product);
        messages = (RelativeLayout) findViewById(R.id.message);

        editShop = (ImageView) findViewById(R.id.edit_shop);
        imvFavorite = (ImageView) findViewById(R.id.imvFavorite);

        imvDeleteShop = (ImageView) findViewById(R.id.delete);

        authentication = new Authentication(this);
        editShop.setOnClickListener(this);

        imvFavorite.setOnClickListener(this);
        products.setOnClickListener(this);
        imvDeleteShop.setOnClickListener(this);
        messages.setOnClickListener(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeRefreshLayout.setRefreshing(true);

        polylines = new ArrayList<>();
        volleyService.makeApiRequest(getShopByIDCallbackCode, getShopByIDUrl, param);


        //my shop recycler
        nearByShopList = new ArrayList<>();


        myShopAdapter = new myShopAdapter(this, nearByShopList, false);

        nearbyShopRecyclerView = (RecyclerView) findViewById(R.id.nearby_shop_recycler);

//        LinearLayout nearbyShopLayout = (LinearLayout) findViewById(R.id.my_shop_layout);


        RecyclerView.LayoutManager nearbyShopLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        nearbyShopRecyclerView.setLayoutManager(nearbyShopLayoutManager);
        nearbyShopRecyclerView.setItemAnimator(new DefaultItemAnimator());

        nearbyShopRecyclerView.setAdapter(myShopAdapter);

        initGoogleAPIClient();//Init Google API Client
        //my shop recycler

    }

    /* Initiate Google API Client  */
    private void initGoogleAPIClient() {
        //Without Google API Client Auto Location Dialog will not work
        mGoogleApiClient = new GoogleApiClient.Builder(ShopDetailActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ShopDetailActivity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else
                showSettingDialog();
        } else
            showSettingDialog();

    }

    /* Show Location Access Dialog */
    private void showSettingDialog() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//Setting priotity of Location request to high
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);//5 sec Time interval for location update
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient to show dialog always when GPS is off

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        updateGPSStatus("GPS is Enabled in your device");
                        drawMapOnLocationChange();
                        Log.i("check_loc", "locations is enable");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ShopDetailActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        Log.e("Settings", "Result OK");
                        drawMapOnLocationChange();

                        updateGPSStatus("GPS is Enabled in your device");
                        //startLocationUpdates();
                        break;
                    case RESULT_CANCELED:
                        Log.e("Settings", "Result Cancel");
                        updateGPSStatus("GPS is Disabled in your device");
                        break;
                }
                break;
        }
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("My_Current_loction", strReturnedAddress.toString());
            } else {
                Log.i("loction_address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("loction_address", "Canont get Address!");
        }
        return strAdd;
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                if (callBackCode == getShopByIDCallbackCode) {

                    JSONObject jsonObject;

                    try {
                        jsonObject = response.getJSONObject("data");
                        shopName = jsonObject.getString("name");
                        phone.setText(jsonObject.getString("phone"));
                        email.setText(jsonObject.getString("email"));
                        website.setText(jsonObject.getString("website"));
                        description.setText(jsonObject.getString("description"));
                        shop_name.setText(jsonObject.getString("name"));
                        Log.i("lat_long", jsonObject.getString("lat") + "," + jsonObject.getString("lng"));
                        if (!(jsonObject.getString("lat")).equals("null")) {
                            lat = Double.valueOf(jsonObject.getString("lat"));
                            Long = Double.valueOf(jsonObject.getString("lng"));

                            shopLocationLatLng = new LatLng(lat, Long);
                            address.setText(getCompleteAddressString(lat, Long));

                            param.put("lat", String.valueOf(lat));
                            param.put("lng", String.valueOf(Long));

                            Log.i("request_near", "" + param);
                            volleyService.makeApiRequest(getNearbyShopCallbackCode, getNearbyShopUrl, param);

                        }

                        setUpMap();
                        if (authentication.checkIfOwner(jsonObject.getString("created_by")) == true) {
                            editShop.setVisibility(View.VISIBLE);
                            imvDeleteShop.setVisibility(View.VISIBLE);
                            imvFavorite.setVisibility(View.GONE);
                        } else {
                            Log.i("track_shop", "not_favorite");
                            if (!(jsonObject.getString("is_favorite")).equals("0")) {
                                is_favorite = true;
                                Log.i("check_fav", (jsonObject.getString("is_favorite")));
                                imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
                            }
                            imvFavorite.setVisibility(View.VISIBLE);
                            editShop.setVisibility(View.GONE);
                            imvDeleteShop.setVisibility(View.GONE);
                        }


                        if (!((jsonObject.getString("image")).equals("null"))) {
                            Log.i("setImg", "true");
                            shopImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(jsonObject.getString("image")));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (callBackCode == createFavoritedShopCallbackCode) {
                    is_favorite = true;
                    imvFavorite.setEnabled(true);
                    imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
                } else if (callBackCode == deleteFavoritedShopCallbackCode) {
                    imvFavorite.setEnabled(true);
                    is_favorite = false;
                    imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_favorite));
                } else if (callBackCode == deleteShopCallbackCode) {
                    imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_favorite));
                } else if (callBackCode == getNearbyShopCallbackCode) {

                    JSONObject jsonObject;
                    JSONArray jsonArray = null;
                    Log.i("get_near", response + "");

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        Log.i("get_nears", jsonArray.length() + "");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Log.i("get_jsonObject", jsonObject + "");

                            Shops shop = new Shops(jsonObject.getString("id"), jsonObject.getString("image"), "+" + jsonObject.getString("product_numbers"), jsonObject.getString("name"));
                            Log.i("get_shop", shop + "");


                            nearByShopList.add(shop);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    myShopAdapter.notifyDataSetChanged();
                    Log.i("near_shops_list", nearByShopList + "");

                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setEnabled(false);

                } else {
                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    intent.putExtra("page_index", 1);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("shop_data_error", error + "");

            }
        };
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        google_map = googleMap;

        drawMapOnLocationChange();
        checkPermissions();
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng pinkHose = new LatLng(lat, Long);

        shopMarker = google_map.addMarker(new MarkerOptions().position(shopLocationLatLng).title(shopName));

        shopMarker.showInfoWindow();

        google_map.moveCamera(CameraUpdateFactory.newLatLng(pinkHose));
        google_map.animateCamera(CameraUpdateFactory.zoomTo(15));

        google_map.setTrafficEnabled(true);


    }

    public void drawMapOnLocationChange() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        google_map.setMyLocationEnabled(true);
        userMarker = null;
        currentLocation = new LatLng(google_map.getMyLocation().getLatitude(),google_map.getMyLocation().getLongitude());
        getRouteToMarker(currentLocation, shopLocationLatLng);



        google_map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {

                if (userMarker != null) {
                    userMarker.remove();
                    shopMarker.showInfoWindow();

                }
                userMarker = google_map.addMarker(new MarkerOptions().position(currentLocation).title(getString(R.string.your_location)));

                userMarker.showInfoWindow();

            }
        });
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //If permission granted show location dialog if APIClient is not null
                    if (mGoogleApiClient == null) {
                        initGoogleAPIClient();
                        showSettingDialog();
                    } else
                        showSettingDialog();


                } else {
                    updateGPSStatus("Location Permission denied.");
                    Toast.makeText(ShopDetailActivity.this, "Location Permission denied.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.product:
                Intent intent = new Intent(getApplication(),ShopProductActivity.class);
                intent.putExtra("shop_id",shop_id);
                intent.putExtra("shop_name",shopName);
                startActivity(intent);
                break;

            case R.id.message:

                Intent intent1 = new Intent(getApplication(),ChatListActivity.class);
                intent1.putExtra(getString(R.string.partner_id),shop_id);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                Toast.makeText(this, "this message", Toast.LENGTH_SHORT).show();

                break;

            case R.id.edit_shop:
                Toast.makeText(this, "edit shop", Toast.LENGTH_SHORT).show();
                intent = new Intent(getApplication(),EditShopActivity.class);
                intent.putExtra("shop_id",shop_id);
                startActivity(intent);
                startActivity(intent);

                break;

            case R.id.imvFavorite:

                imvFavorite.setEnabled(false);
                if (is_favorite == true){

                    alertRemoveFavoriteMessage();
                }
                else {
                    if (authentication.checkIfLogedIn() == true) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", shop_id);
                        param.put("type", getString(R.string.shop_type));
                        Log.i("post_fav", "" + param);

                        volleyService.makeApiRequest(createFavoritedShopCallbackCode, createFavoritedShopUrl, param);
                    } else {

                        Intent intent2 = new Intent(getApplication(), SignInActivity.class);
                        intent2.putExtra("is_favorite", true);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent2.putExtra("type", getString(R.string.shop_type));
                        intent2.putExtra("item_id", shop_id);
                        startActivity(intent2);
                        finish();

                    }
                }
                break;

            case R.id.delete:

                alertDelet();
//                new MaterialDialog.Builder(this)
//                        .title("Are you sure you want to delete?")
//                        .positiveText("Yes")
//                        .showListener(new DialogInterface.OnShowListener() {
//                            @Override
//                            public void onShow(DialogInterface dialog) {
//                                Toast.makeText(getApplication(), "Yes", Toast.LENGTH_SHORT).show();
//
//                            }
//                        })
//                        .cancelListener(new DialogInterface.OnCancelListener() {
//                            @Override
//                            public void onCancel(DialogInterface dialog) {
//                            }
//                        })
//                        .dismissListener(new DialogInterface.OnDismissListener() {
//                            @Override
//                            public void onDismiss(DialogInterface dialog) {
//                            }
//                        })
//                        .show();
//

                break;
        }
    }

    public void setUpMap(){
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_view);
        mapFragment.getMapAsync(this);

    }

    public void alertDelet(){

        new AlertDialog.Builder(this)

                .setMessage("Are you sure you want to delete?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        param.put("user_id",authentication.getUserPreference().getString("id",null));
                        param.put("shop_id",shop_id);
                        volleyService.makeApiRequest(deleteShopCallbackCode,deleteShopUrl,param);
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    public void alertRemoveFavoriteMessage(){

        new android.support.v7.app.AlertDialog.Builder(this)

                .setMessage(getString(R.string.confirm_remove_favorite_product))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", shop_id);
                        param.put("type", getString(R.string.shop_type));

                        Log.i("post_fav",""+param);
                        volleyService.makeApiRequest(deleteFavoritedShopCallbackCode, deleteFavoritedShopUrl, param);

                    }})
                .setNegativeButton(android.R.string.no, null).show();




    }

    @Override
    public void onRoutingFailure(RouteException e) {

        Toast.makeText(getApplication(), "check 1", Toast.LENGTH_SHORT).show();

        // The Routing request failed
//        progressDialog.dismiss();
        if(e != null) {
            Log.i( "Error: " , e.getMessage());
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }else {
            Log.i( "Error: " ,"Something went wrong, Try again");

            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

        Toast.makeText(getApplication(), "check 2", Toast.LENGTH_SHORT).show();

        Log.i("checkstart","onRoutingStart");
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {

        Log.i("check_distance",""+route.get(i).getDistanceText());
        if (google_map != null) {

            if (polylines.size() > 0) {
                for (Polyline poly : polylines) {
                    poly.remove();
                }
            }
            //add route(s) to the map.
            for (i = 0; i < route.size(); i++) {

                //In case of more than 5 alternative routes
                drawRouteOnMap(google_map, route.get(i).getPoints(),i);

            }
        }
    }

    public void onRoutingCancelled() {

        Toast.makeText(getApplication(), "check 4", Toast.LENGTH_SHORT).show();
    }

    public void getRouteToMarker(LatLng currentLocation, LatLng shopLocationLatLng){

        Log.i("check_latlng",currentLocation+""+shopLocationLatLng);

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(false)
                .waypoints(new LatLng(currentLocation.latitude, currentLocation.longitude), shopLocationLatLng)
                .build();
        routing.execute();

    }

    //3
    public  void drawRouteOnMap(GoogleMap map, List<LatLng> positions, int routeIndex) {

        int colorIndex = routeIndex % COLORS.length;
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(COLORS[colorIndex]));
        polyOptions.width(13 + routeIndex * 3);
        polyOptions.addAll(positions);
        Polyline polyline = map.addPolyline(polyOptions);
        polylines.add(polyline);
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Register broadcast receiver to check the status of GPS
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Unregister receiver on destroy
        if (gpsLocationReceiver != null)
            unregisterReceiver(gpsLocationReceiver);
    }

    //Run on UI
    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            showSettingDialog();
        }
    };

    /* Broadcast receiver to check status of GPS */
    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    updateGPSStatus("GPS is Enabled in your device");
                    drawMapOnLocationChange();
                    Log.i("check_loc","locations is enable");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
                    updateGPSStatus("GPS is Disabled in your device");
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ShopDetailActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(ShopDetailActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(ShopDetailActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }

    //Method to update GPS status text
    private void updateGPSStatus(String status) {

        Log.i("location_change",status);
//        gps_status.setText(status);
    }


}
