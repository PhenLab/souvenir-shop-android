package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{

    private AppCompatButton btnSingUp;

    private EditText firstName, lastName, userName,
            phone, email, password, confirmedPassword;
    private TextInputLayout passInputLayout;

    SwipeRefreshLayout swipeRefreshLayout;

    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int signUpCallbackCode = 1;
    private String signUpUrl = "/register";
    private String is_edit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.sign_up));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnSingUp = (AppCompatButton)findViewById(R.id.btn_sign_up);
        btnSingUp.setOnClickListener(this);

        firstName = (EditText)findViewById(R.id.first_name);
        lastName = (EditText)findViewById(R.id.last_name);
        userName = (EditText)findViewById(R.id.user_name);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        confirmedPassword = (EditText)findViewById(R.id.conf_password);

        is_edit = getIntent().getStringExtra("edit_user");
        if (is_edit == "true" || is_edit.equals("true")){
//            Toast.makeText(this, "edit_user "+ is_edit, Toast.LENGTH_SHORT).show();

            authentication = new Authentication(getApplication());
            firstName.setText(authentication.getUserPreference().getString("first_name",""));
            lastName.setText(authentication.getUserPreference().getString("last_name",""));
            userName.setText(authentication.getUserPreference().getString("user_name",""));
            phone.setText(authentication.getUserPreference().getString("phone_name",""));
            email.setText(authentication.getUserPreference().getString("email_name",""));

            signUpUrl = "/update-user";
        }

        //sever
        initVolleyCallback();

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        volleyService = new VolleyService(resultCallback,getApplication());

    }

    private void initVolleyCallback(){
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                SharedPreferences pref = getApplicationContext().getSharedPreferences(userData,MODE_PRIVATE); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();

                try {
                    editor.putString("id",response.getString("id"));
                    editor.putString("first_name",response.getString("first_name"));
                    editor.putString("last_name",response.getString("last_name"));
                    editor.putString("user_name",response.getString("user_name"));
                    editor.putString("phone",response.getString("phone"));
                    editor.putString("email",response.getString("email"));
                    editor.putString("image",response.getString("image"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                editor.commit();

                Log.i("user_data_save",pref.getString("id","null")+","
                        +pref.getString("first_name","null")+""+pref.getString("last_name","null")+","
                        +pref.getString("user_name","null")+""+pref.getString("phone","null"));
                Log.i("response",response+"");

                swipeRefreshLayout.setRefreshing(false);
                startActivity(new Intent(getApplication(), MainActivity.class));

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                Log.i("error",error+"");
//                Toast.makeText(SignUpActivity.this, "this is error "+error, Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_up:
                swipeRefreshLayout.setRefreshing(true);
                param.put("first_name",firstName.getText().toString());
                param.put("last_name",lastName.getText().toString());
                param.put("user_name",userName.getText().toString());
                param.put("phone",phone.getText().toString());
                param.put("email",email.getText().toString());
                if (is_edit != "true" || !is_edit.equals("true")) {

                    param.put("password", password.getText().toString());
                    param.put("password_confirmation", confirmedPassword.getText().toString());
                }
//                Toast.makeText(this, ""+param, Toast.LENGTH_SHORT).show();

                volleyService.makeApiRequest(signUpCallbackCode, signUpUrl,param);
                //passInputLayout.setError("password miss match!");
                break;

        }
    }
}
