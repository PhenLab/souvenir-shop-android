package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/26/2018.
 */

public class Messages {

    private String created_at;
    private String message;
    private String my_id;
    private String my_type;
    private String partner_id;
    private String partner_type;
    private String customerId_shopId;

    public Messages(){

    }

    public Messages(String created_at, String message, String my_id, String my_type, String partner_id, String partner_type, String customerId_shopId) {
        this.created_at = created_at;
        this.message = message;
        this.my_id = my_id;
        this.my_type = my_type;
        this.partner_id = partner_id;
        this.partner_type = partner_type;
        this.customerId_shopId = customerId_shopId;

    }

    public String getCustomerId_shopId() {
        return customerId_shopId;
    }

    public void setCustomerId_shopId(String customerId_shopId) {
        this.customerId_shopId = customerId_shopId;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMy_id() {
        return my_id;
    }

    public void setMy_id(String my_id) {
        this.my_id = my_id;
    }

    public String getMy_type() {
        return my_type;
    }

    public void setMy_type(String my_type) {
        this.my_type = my_type;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_type() {
        return partner_type;
    }

    public void setPartner_type(String partner_type) {
        this.partner_type = partner_type;
    }
}
