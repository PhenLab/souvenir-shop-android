package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditProductActivity extends AppCompatActivity implements View.OnClickListener , MaterialSpinner.OnItemSelectedListener{


    Toolbar toolbar;
    private static final int GET_IMAGE_FROM_CAMERA_CODE = 1;
    private static final int GET_IMAGE_FROM_GALLERY_CODE = 2;

    //image
    private ImageConverter imageConverter;
    private String imageBase64String;
    private Base64 imageBitmap;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int createProductCallbackCode = 1;
    private String createProductUrl = "/product/update";
    private int getCategoryListCallbackCode = 2;
    private String getCategoryListUrl = "/category/list";

    String shop_id = null;
    private MaterialSpinner selectCategory;
    private List<String> categoryNameList = new ArrayList<String>();
    private List<String> categoryIdList = new ArrayList<String>();

    private EditText edtName, edtQuantity, edtPrice, edtDescription;
    private ImageView imvImage;
    private AppCompatButton btnSave;

    private String getImageBase64String, product_id,name, category_id, description, price, imageBase64,quantity;
    private Uri ImgUri;
    private Bitmap imgBitmap;

    private String selectedCategoryId = "";
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);



        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.edit_product));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        product_id = getIntent().getStringExtra("product_id");
        category_id = getIntent().getStringExtra("category_id");
        price = getIntent().getStringExtra("price");
        quantity = getIntent().getStringExtra("quantity");
        description = getIntent().getStringExtra("description");
        imageBase64 = getIntent().getStringExtra("image");
        name = getIntent().getStringExtra("name");


        categoryNameList.add(getResources().getString(R.string.choose_a_category));
        categoryIdList.add("0");

        selectCategory = (MaterialSpinner)findViewById(R.id.select_category);

        selectCategory.setOnItemSelectedListener(this);

        edtName = (EditText)findViewById(R.id.product_name);
        edtQuantity = (EditText)findViewById(R.id.quantity);
        edtPrice = (EditText)findViewById(R.id.price);
        edtDescription = (EditText)findViewById(R.id.description);
        imvImage = (ImageView) findViewById(R.id.image);
        btnSave = (AppCompatButton) findViewById(R.id.btn_save_product);
        btnSave.setOnClickListener(this);
        imvImage.setOnClickListener(this);
        imageConverter = new ImageConverter(this);
        authentication = new Authentication(this);

        //set value to blank
        edtName.setText(name);
        edtPrice.setText(price);
        edtQuantity.setText(quantity);
        edtDescription.setText(description);
        Log.i("image_base",imageBase64);
        if (!(imageBase64).equals("null")) {
            imageBase64String = imageBase64;
            imvImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(imageBase64));
        }
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));


        selectedCategoryId = category_id;
        initVolleyCallback();
        volleyService = new VolleyService(resultCallback,this);

        loadProduct();
    }

    private void loadProduct() {

        swipeRefreshLayout.setRefreshing(true);
        volleyService.makeApiRequest(getCategoryListCallbackCode,getCategoryListUrl,param);

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                if (callBackCode == getCategoryListCallbackCode) {
                    JSONArray jsonArray = null;
                    JSONObject jsonObject;

                    try {
                        jsonArray = response.getJSONArray("data");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Log.i("get_category", "" + jsonObject);
                            categoryNameList.add(jsonObject.getString("name"));
                            categoryIdList.add(jsonObject.getString("id"));

                        }

                        selectCategory.setItems(categoryNameList);
                        for ( int i= 0; i<categoryIdList.size();i++) {
                            if ((categoryIdList.get(i)).equals(category_id)) {
                                Log.i("index", "" + i);

                                selectCategory.setSelectedIndex(i);
                            }
                        }
                        Log.i("category_name", "" + categoryNameList);
                        Log.i("category_id", "" + categoryIdList);

                        swipeRefreshLayout.setRefreshing(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {

                    try {
                        String id = response.getString("id");
                        Intent intent = new Intent(getApplication(),ProductDetailActivity.class);
                        intent.putExtra("product_id",id);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        startActivity(intent);

                        swipeRefreshLayout.setRefreshing(false);
                        Log.i("get_product",id+"");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                Log.i("get_error",""+ error);
            }
        };

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        selectedCategoryId = categoryIdList.get(position);
//
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.image:
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(EditProductActivity.this);
                builderSingle.setTitle(getResources().getString(R.string.choose_option));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditProductActivity.this, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add(getResources().getString(R.string.camera));
                arrayAdapter.add(getResources().getString(R.string.gallery));


                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        if (which == 0){
                            launchCameraClick();
                        }
                        else {
                            openGalleryClick();
                        }
                    }
                });
                builderSingle.show();

                break;

            case R.id.btn_save_product:
                swipeRefreshLayout.setRefreshing(true);
                param.clear();
                param.put("name",edtName.getText().toString());
                param.put("price", edtPrice.getText().toString());
                param.put("quantity", edtQuantity.getText().toString());
                param.put("description", edtDescription.getText().toString());
                param.put("id",product_id);
                param.put("user_id",authentication.getUserPreference().getString("id",null));
                param.put("category_id",selectedCategoryId);
                param.put("image",imageBase64String);

                Log.i("post_data",""+param);
                volleyService.makeApiRequest(createProductCallbackCode,createProductUrl,param);
                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_IMAGE_FROM_GALLERY_CODE && resultCode == RESULT_OK){

            ImgUri = data.getData();
            imgBitmap = imageConverter.loadFitImage(ImgUri,300,300);

            imvImage.setImageBitmap(imgBitmap);
            //convert from bitmap to base64 string
            imageBase64String = imageConverter.convertFromBitmapToBase64(imgBitmap);

            Log.i("base64",","+imageBase64String+",");

        }
        else if (requestCode == GET_IMAGE_FROM_CAMERA_CODE && resultCode == RESULT_OK){

            ImgUri = (Uri) data.getExtras().get("data");
            imgBitmap = imageConverter.loadFitImage(ImgUri,300,300);
            imvImage.setImageBitmap(imgBitmap);
            imageBase64String = imageConverter.convertFromBitmapToBase64(imgBitmap);


        }
    }


    //camera
    public void launchCameraClick(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,GET_IMAGE_FROM_CAMERA_CODE);

    }

    //gallery
    public void openGalleryClick(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,GET_IMAGE_FROM_GALLERY_CODE);

    }

}
