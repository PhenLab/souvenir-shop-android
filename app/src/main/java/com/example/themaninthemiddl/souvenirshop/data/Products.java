package com.example.themaninthemiddl.souvenirshop.data;

import android.widget.ImageView;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class Products {
    private String product_img;
    private String name;
    private String price;
    private String category;
    private String shop_name, product_id,shop_id;

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Products(String product_id, String product_img, String name, String price, String category, String shop_name) {
        this.product_img = product_img;
        this.name = name;
        this.price = price;
        this.category = category;
        this.product_id =product_id;
        this.shop_name = shop_name;
    }

    public String getProduct_img() {
        return product_img;
    }

    public void setProduct_img(String product_img) {
        this.product_img = product_img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }
}
