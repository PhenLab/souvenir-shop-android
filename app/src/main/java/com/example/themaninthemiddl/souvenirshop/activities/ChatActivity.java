package com.example.themaninthemiddl.souvenirshop.activities;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatAdapter;
import com.example.themaninthemiddl.souvenirshop.data.ChatMessages;
import com.example.themaninthemiddl.souvenirshop.data.Messages;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;


public class ChatActivity extends AppCompatActivity {

    Toolbar toolbar;

    ChatAdapter chatAdapter;
    EditText edtMessage;
    Button btnSend;
    String shop_owner_id = "0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.chat));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edtMessage = (EditText)findViewById(R.id.ed_text_chatbox);
        btnSend = (Button)findViewById(R.id.btn_send_message);

        shop_owner_id = getIntent().getStringExtra("shop_owner_id");
        shop_owner_id = "1";

//my shop recycler
        final ArrayList<ChatMessages> chatList = new ArrayList<>();


        chatAdapter = new ChatAdapter(this, chatList,true);
        RecyclerView chatRecyclerView = (RecyclerView)findViewById(R.id.reyclerview_message_list);

        RecyclerView.LayoutManager myShopLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        chatRecyclerView.setLayoutManager(myShopLayoutManager);
        chatRecyclerView.setItemAnimator(new DefaultItemAnimator());

        chatRecyclerView.setAdapter(chatAdapter);

        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("chats");

        Query query = mDatabase.orderByChild("customerId_shopId").equalTo("2_33");

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.i("get_value",""+dataSnapshot.getValue());
                Log.i("get_key",""+dataSnapshot.getKey());
                Log.i("get_child",""+dataSnapshot.getChildren());

                Messages message = dataSnapshot.getValue(Messages.class);
//                ChatMessages chatMessage = new ChatMessages(message.getMessage(),message.getPartner_id(),)

                Log.i("check_data1",""+message.getCreated_at());
//
//                Log.i("check_data2",""+message.getMessage());




            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Log.i("data_change",""+dataSnapshot);


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Log.i("read_data",""+mDatabase.child("chats"));

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                Map<String, Messages> messagesHashMap = new HashMap<>();
                Messages messages = new Messages("541912", "Hello Customer","2","1","33","2","2_33");

                mDatabase.child(getRandomString()).setValue(messages);
                Log.i("inserted",""+messagesHashMap);

            }
        });

    }

    public String getRandomString() {

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("EE,d MMMM yyyy h:mma", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        Log.i("get_mili","mili"+System.currentTimeMillis()+","+formattedDate);
         return String.valueOf(System.currentTimeMillis());

    }

    public String converMiliSecondToDate() {
//        long offset = Timestamp.valueOf("2012-01-01 00:00:00").getTime();
//        long end = Timestamp.valueOf("2013-01-01 00:00:00").getTime();
//        long diff = end - offset + 1;
//        Timestamp rand = new Timestamp(offset + (long)(Math.random() * diff));
//        return rand.toString();

        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMM d,yyyy h:mm,a", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        Log.i("get_mili","mili"+System.currentTimeMillis()+","+formattedDate);
        return String.valueOf(System.currentTimeMillis());

    }


}
