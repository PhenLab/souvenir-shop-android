package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class FavoriteShopAdapter extends RecyclerView.Adapter<FavoriteShopAdapter.MyViewHolder> {

    private Context mContext;
    private List<Shops> shopList;
    private ImageConverter imageConverter;
    private String base64String;


    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;

    private String deleteFavoritedShopUrl = "/favorite/delete";
    private int deleteFavoritedShopCallbackCode = 10;

    private productAdapter productAdapter;
    private List<Products> relatedProductList = new ArrayList<>();

    CollapsingToolbarLayout toolbarLayout;
    private RecyclerView relateProductRecyclerView;
    AppCompatButton btnContact;
    String shop_id = null;
    int position = -1;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_shop_card, parent, false);
        itemView.setLayoutParams(new TableLayout.LayoutParams(0, 800,1f));
        initVolleyService();
        authentication = new Authentication(mContext);
        volleyService = new VolleyService(resultCallback , mContext);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Shops shops = shopList.get(position);
        holder.product_number.setText(shops.getProduct_number());
        holder.shop_name.setText(shops.getShop_name());
        if (!shops.getShop_img().equals("null")) {
            Log.i("check_img",shops.getShop_img());
            holder.shop_img.setImageBitmap(imageConverter.convertFromBase64ToBitmap(shops.getShop_img()));
        }

        // loading album cover using Glide library
//        Glide.with(mContext).load().into(holder.shop_img);
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView  shop_name, product_number;
        public ImageView shop_img,favorite;
        RelativeLayout rlyDelete;

        public MyViewHolder(View view) {
            super(view);
//            favorite = (ImageView) view.findViewById(R.id.favorite);
            shop_img = (ImageView) view.findViewById(R.id.img_shop);
            shop_name = (TextView) view.findViewById(R.id.shop_name);
            rlyDelete = (RelativeLayout)view.findViewById(R.id.delete_favorite_shop);
            rlyDelete.setOnClickListener(this);
            product_number = (TextView)view.findViewById(R.id.product_number);
            shop_img.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            position = this.getAdapterPosition();
            Shops shops = shopList.get(this.getAdapterPosition());

            shop_id = shops.getId();

            switch (view.getId()) {
                case R.id.img_shop:

                    Intent intent = new Intent(mContext, ShopDetailActivity.class);
                    intent.putExtra("SHOP_ID", shop_id);
                    mContext.startActivity(intent);

                    break;

                case R.id.contact:

                    Toast.makeText(mContext, "this is contact " , Toast.LENGTH_LONG).show();
                    break;

                case R.id.delete_favorite_shop:

                    alertRemoveFavoriteMessage();
                    Toast.makeText(mContext, "delete" +position , Toast.LENGTH_LONG).show();
                    break;
            }

        }
    }

    public FavoriteShopAdapter(Context mContext, List<Shops> shopList) {

        this.mContext = mContext;
        this.shopList = shopList;

    }

    public void alertRemoveFavoriteMessage(){

        new android.support.v7.app.AlertDialog.Builder(mContext)

                .setMessage(mContext.getString(R.string.confirm_remove_favorite_product))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", shop_id);
                        param.put("type", mContext.getString(R.string.shop_type));

                        Log.i("post_fav",""+param);
                        volleyService.makeApiRequest(deleteFavoritedShopCallbackCode, deleteFavoritedShopUrl, param);


                    }})

                .setNegativeButton(android.R.string.no, null).show();

    }


    public void initVolleyService() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {
                if (callBackCode == deleteFavoritedShopCallbackCode){
                    shopList.remove(position);
                    Log.i("check_favorite_data",""+shopList);
                    notifyDataSetChanged();

                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Toast.makeText(mContext, "remove error", Toast.LENGTH_SHORT).show();
            }
        };
    }


}
