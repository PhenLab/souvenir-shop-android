package com.example.themaninthemiddl.souvenirshop.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class CreateShopActivity extends AppCompatActivity  implements View.OnClickListener, OnMapReadyCallback {

    private static final int GET_IMAGE_FROM_CAMERA_CODE = 1;
    private static final int GET_IMAGE_FROM_GALLERY_CODE = 2;

    private Uri shopImgUri;
    private InputStream imgInputStream;
    private Bitmap shopImgBitmap;

    private Toolbar toolbar;
    private EditText edtShopName, edtEmail, edtPhone, edtWebsite, edtLat, edtLong,edtDescription;
    private ImageView profile;
    private AppCompatButton btnPickLocation, btnCreateShop;

    //image
    private ImageConverter imageConverter;

    private String imageBase64String;

    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int createShopCallbackCode = 1;
    private String createShopUrl = "/shop/add";
    private int getShopByIDCallbackCode = 2;
    private String getShopByIDUrl = "/get-shop-by-id";
    int PLACE_PICKER_REQUEST = 3;

    String shop_id = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_shop);


        imageConverter = new ImageConverter(this);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.create_shop));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                startActivity(new Intent(getApplication(),MainActivity.class));

            }
        });

        edtShopName = (EditText)findViewById(R.id.shop_name);
        edtPhone = (EditText)findViewById(R.id.phone);
        edtEmail = (EditText)findViewById(R.id.email);
        edtWebsite = (EditText)findViewById(R.id.website);
        edtLat = (EditText)findViewById(R.id.lat);
        edtLong = (EditText)findViewById(R.id.Long);
        edtDescription = (EditText)findViewById(R.id.description);

        profile = (ImageView) findViewById(R.id.profile);
        btnCreateShop = (AppCompatButton) findViewById(R.id.btn_ceate_shop);
        btnPickLocation = (AppCompatButton) findViewById(R.id.btn_pick_location);

        profile.setOnClickListener(this);
        btnCreateShop.setOnClickListener(this);
        btnPickLocation.setOnClickListener(this);

        authentication = new Authentication(this);

        initializeVolleyCallback();
        volleyService = new VolleyService(resultCallback,this);


    }

    private void initializeVolleyCallback() {
        resultCallback =new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {


                JSONObject jsonObject;

                try {
                    Intent intent = new Intent(getApplication(), ShopDetailActivity.class);
                    intent.putExtra("SHOP_ID", response.getString("data"));
                    startActivity(intent);
                    finish();


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Toast.makeText(CreateShopActivity.this, "create error "+ error, Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.profile:

                AlertDialog.Builder builderSingle = new AlertDialog.Builder(CreateShopActivity.this);
                builderSingle.setTitle(getResources().getString(R.string.choose_option));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(CreateShopActivity.this, android.R.layout.select_dialog_singlechoice);
                arrayAdapter.add(getResources().getString(R.string.camera));
                arrayAdapter.add(getResources().getString(R.string.gallery));


                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        if (which == 0){
                            launchCameraClick();
                        }
                        else {
                            openGalleryClick();
                        }
                    }
                });
                builderSingle.show();

                break;

            case R.id.btn_ceate_shop:
                param.clear();
                param.put("name", edtShopName.getText().toString());
                param.put("phone", edtPhone.getText().toString());
                param.put("email", edtEmail.getText().toString());
                param.put("website", edtWebsite.getText().toString());
                param.put("lat",edtLat.getText().toString());
                param.put("Long",edtLong.getText().toString());
                param.put("description",edtDescription.getText().toString());
                param.put("user_id",authentication.getUserPreference().getString("id",null));
                param.put("image",imageBase64String);

                Log.i("post_shop",""+param);
                volleyService.makeApiRequest(createShopCallbackCode,createShopUrl,param);
                break;

            case R.id.btn_pick_location:

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_IMAGE_FROM_GALLERY_CODE && resultCode == RESULT_OK){

            shopImgUri = data.getData();
                shopImgBitmap = imageConverter.loadFitImage(shopImgUri,300,300);

                profile.setImageBitmap(shopImgBitmap);
                //convert from bitmap to base64 string
                imageBase64String = imageConverter.convertFromBitmapToBase64(shopImgBitmap);

                Log.i("base64",","+imageBase64String+",");

        }
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                edtLat.setText(String.valueOf(place.getLatLng().latitude));
                edtLong.setText(String.valueOf(place.getLatLng().longitude));

                String toastMsg = String.format("Place: %s", place.getName());
            }
        }
        else if (requestCode == GET_IMAGE_FROM_CAMERA_CODE && resultCode == RESULT_OK){

            shopImgUri = (Uri) data.getExtras().get("data");
            shopImgBitmap = imageConverter.loadFitImage(shopImgUri,300,300);
            profile.setImageBitmap(shopImgBitmap);
            imageBase64String = imageConverter.convertFromBitmapToBase64(shopImgBitmap);


        }
    }

    //camera
    public void launchCameraClick(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,GET_IMAGE_FROM_CAMERA_CODE);

    }

    //gallery
    public void openGalleryClick(){

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,GET_IMAGE_FROM_GALLERY_CODE);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
