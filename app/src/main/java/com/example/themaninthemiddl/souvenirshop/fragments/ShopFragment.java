package com.example.themaninthemiddl.souvenirshop.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.CreateShopActivity;
import com.example.themaninthemiddl.souvenirshop.adapters.myShopAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.otherShopAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.Shops;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private RecyclerView myShopRecyclerView;

    private LinearLayout myShopLayout;
    private myShopAdapter myShopAdapter;
    private otherShopAdapter otherShopAdapter;

    private List<Shops> myShopList;
    private List<Shops> otherShopList;

    private ImageView addShop;


    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getMyShopCallbackCode = 1;
    private String getMyShopUrl = "/get-shop-by-owner-id";

    private int getOtherShopCallbackCode = 2;
    private String getOtherShopUrl = "/get-other-shop-by-owner-id";
    private FusedLocationProviderClient mFusedLocationClient;
    LatLng currentLocation;
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_shop, container, false);


        //other shop recycler
        otherShopList = new ArrayList<>();
        otherShopAdapter = new otherShopAdapter(getActivity(),otherShopList);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);


        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(otherShopAdapter);

        //my shop recycler
        myShopList = new ArrayList<>();


        myShopAdapter = new myShopAdapter(getActivity(), myShopList,true);
        myShopRecyclerView = (RecyclerView)view.findViewById(R.id.my_shop_recycler);
        myShopLayout = (LinearLayout)view.findViewById(R.id.my_shop_layout);


        RecyclerView.LayoutManager myShopLayoutManager = new GridLayoutManager(getActivity(), 2);
        myShopRecyclerView.setLayoutManager(myShopLayoutManager);
        myShopRecyclerView.setItemAnimator(new DefaultItemAnimator());

        myShopRecyclerView.setAdapter(myShopAdapter);
        //my shop recycler


        addShop = (ImageView)view.findViewById(R.id.add_new_shop);
        addShop.setOnClickListener(this);



        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        swipeRefreshLayout.setRefreshing(true);

        //check if login
        authentication = new Authentication(getActivity());
        if(authentication.checkIfLogedIn() != true){
            myShopLayout.setVisibility(View.GONE);
        }

        //server
        initVolleyCallback();
        param.put("user_id",authentication.getUserPreference().getString("id",null));

        volleyService = new VolleyService(resultCallback, getActivity());
        volleyService.makeApiRequest(getMyShopCallbackCode,getMyShopUrl,param);
        volleyService.makeApiRequest(getOtherShopCallbackCode,getOtherShopUrl,param);

        return view;
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {


                Log.i("shops response",""+response);

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getMyShopCallbackCode) {
                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Shops shop = new Shops(jsonObject.getString("id"), jsonObject.getString("image"),"+" +jsonObject.getString("product_numbers"), jsonObject.getString("name"));

                            myShopList.add(shop);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    myShopAdapter.notifyDataSetChanged();
                }
                else {

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Shops shop = new Shops(jsonObject.getString("id"), jsonObject.getString("image"), "+" +jsonObject.getString("product_numbers"), jsonObject.getString("name"));

                            otherShopList.add(shop);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    otherShopAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Toast.makeText(getActivity(), "error "+ error, Toast.LENGTH_SHORT).show();

            }
        };
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_new_shop:

                startActivity(new Intent(getActivity(), CreateShopActivity.class));
                getActivity().finish();
                break;
        }
    }

    public LatLng getCurrentLocation() {
        // ...

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(getActivity(), "go to get latlong", Toast.LENGTH_SHORT).show();
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(android.location.Location location) {
                    currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                    Toast.makeText(getActivity(), "got loc"+currentLocation, Toast.LENGTH_SHORT).show();
                }
            });


        }

        return currentLocation;
    }

}
