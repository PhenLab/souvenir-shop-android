package com.example.themaninthemiddl.souvenirshop.adapters;

/**
 * Created by The Man In The Middl on 4/30/2018.
 */

public interface RecyclerViewItemClickListener {

    void onRecyclerViewItemClick(int position);

}
