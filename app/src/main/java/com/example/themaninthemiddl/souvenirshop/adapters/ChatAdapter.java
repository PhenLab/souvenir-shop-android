package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ChatMessages;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private Context mContext;
    private List<ChatMessages> chatList;
    private ImageConverter imageConverter;
    private String base64String;
    boolean isCustomise = false;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private int storeViewID = 0;
    Authentication authentication;

    private int getShopByIDCallbackCode = 20;
    private String getShopByIDUrl = "/get-shop-by-id";
    private VolleyService volleyService;
    //server
    Map<String, String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        authentication = new Authentication(mContext);

        View itemView;
        if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            storeViewID = VIEW_TYPE_MESSAGE_RECEIVED;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
        }
        else {
            storeViewID = VIEW_TYPE_MESSAGE_SENT;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ChatMessages chat = chatList.get(position);
        if (holder.getItemViewType() == VIEW_TYPE_MESSAGE_RECEIVED){

            holder.tvMesage.setText(chat.getMessageText());
            holder.tvMessageTime.setText(chat.getMessageTime());
            holder.tvMesageSender.setText(chat.getMessageSender());

            if (!chat.getSenderImage().equals("null")) {
                Log.i("check_img", chat.getSenderImage());
                holder.senderImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(chat.getSenderImage()));
            }
        }
        else {
            holder.tvMesage.setText(chat.getMessageText());
            holder.tvMessageTime.setText(chat.getMessageTime());
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }


    @Override
    public int getItemViewType(int position) {
        ChatMessages chatMessages = chatList.get(position);

        volleyService = new VolleyService(resultCallback,mContext);

        if (chatMessages.getMyType() == mContext.getString(R.string.shop_owner_type)){
            param.put("shop_id",chatMessages.getMyID());
            volleyService.makeApiRequest(getShopByIDCallbackCode,getShopByIDUrl,param);
        }
        if (chatMessages.getMyID().equals(authentication.getUserPreference().getString("id",null) )) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        public TextView  tvMesage, tvMessageTime, tvMesageSender;
        public ImageView senderImage;

        public MyViewHolder(View view) {
            super(view);

            if (getItemViewType() == VIEW_TYPE_MESSAGE_RECEIVED) {
                senderImage = (ImageView) view.findViewById(R.id.sender_image);
                tvMessageTime = (TextView) view.findViewById(R.id.tv_sender_message_time);
                tvMesage = (TextView) view.findViewById(R.id.tv_sender_message);
                senderImage.setOnLongClickListener(this);
                tvMesage.setOnLongClickListener(this);
            }
            else {

                tvMessageTime = (TextView) view.findViewById(R.id.tv_my_message_time);
                tvMesage = (TextView) view.findViewById(R.id.tv_my_message);
                tvMesage.setOnLongClickListener(this);
            }
        }


//        @Override
//        public void onClick(View view) {
//
//
//            switch (view.getId()) {
//                case R.id.img_shop:
//                    ChatMessages chatMessages = chatList.get(this.getAdapterPosition());
//
//                    Intent intent = new Intent(mContext, ShopDetailActivity.class);
//                    mContext.startActivity(intent);
//
//                    break;
//
//                case R.id.contact:
//
//                    Toast.makeText(mContext, "this is contact " , Toast.LENGTH_LONG).show();
//                    break;
//            }
//
//        }

        @Override
        public boolean onLongClick(View view) {
            return false;
        }
    }

    public ChatAdapter(Context mContext, List<ChatMessages> chatList, boolean isCustomise) {

        this.isCustomise = isCustomise;
        this.mContext = mContext;
        this.chatList = chatList;

    }

}
