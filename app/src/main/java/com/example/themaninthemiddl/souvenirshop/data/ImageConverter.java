package com.example.themaninthemiddl.souvenirshop.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import static android.graphics.BitmapFactory.decodeStream;

/**
 * Created by The Man In The Middl on 5/3/2018.
 */

public class ImageConverter {

    Bitmap bitmap;
    static Context context;

    public ImageConverter(Context context) {
        this.context = context;
    }

    public static String convertFromBitmapToBase64(Bitmap bitmap) {

        //encode image to base64 string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return imageString;

    }

    public static Bitmap convertFromBase64ToBitmap(String base64String) {

        //decode base64 string to image
        base64String.trim();
        byte[] imageBytes ;
        imageBytes = Base64.decode(base64String, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        return decodedImage;

    }
    public static Uri convertFromBase64ToUri(String base64String) {

        //decode base64 string to image
        base64String.trim();
        byte[] imageBytes ;
        imageBytes = Base64.decode(base64String, Base64.DEFAULT);
        Uri imageConvertToUri = Uri.parse(base64String);
        return imageConvertToUri;

    }

    public static Bitmap scaleImageDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static Bitmap loadFitImage(Uri imageUri, int targetWidth, int targetHeight) {

        // Get image's size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            decodeStream(context.getContentResolver().openInputStream(imageUri), null, options);
            int imageWidth = options.outWidth;
            int imageHeight = options.outHeight;

            // Caculate scale factor
            int scaleFactor = Math.min(imageWidth / targetWidth, imageHeight / targetHeight);

            // Modify decode option
            options.inJustDecodeBounds = false;
            options.inSampleSize = scaleFactor;

            return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

    }

}
