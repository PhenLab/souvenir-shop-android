package com.example.themaninthemiddl.souvenirshop.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.ChatListAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.myShopAdapter;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;

import java.util.ArrayList;
import java.util.List;

public class ChatListActivity extends AppCompatActivity {


    Toolbar toolbar;
    List<AllChatList> allChatLists;
    ChatListAdapter chatListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.shop_chat_list));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        //my shop recycler
        allChatLists = new ArrayList<>();


        chatListAdapter = new ChatListAdapter(getApplication(), allChatLists);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view_chat_list);


        RecyclerView.LayoutManager chatListLayoutManager = new LinearLayoutManager(getApplication(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(chatListLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(chatListAdapter);
        //my shop recycler

        setTestData();
    }

    public void setTestData(){

        AllChatList allChatList = new AllChatList("Mesage","Name","5:00pm","33","R.drawable.ic_tab_shop","2");

        allChatLists.add(allChatList);

        chatListAdapter.notifyDataSetChanged();
    }
}
