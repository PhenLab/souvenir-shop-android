package com.example.themaninthemiddl.souvenirshop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.RecyclerViewItemClickListener;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements RecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private productAdapter productAdapter;
    private List<Products> productList;

    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getProductListCallbackCode = 1;
    private String getProductListUrl = "/get-product-list";

    private ImageView addProduct, searchFilter;
    private ImageConverter imageConverter;

    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);

        productList = new ArrayList<>();

        productAdapter = new productAdapter(getActivity(), productList,true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(productAdapter);

        authentication = new Authentication(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        initVolleyCallback();
        imageConverter = new ImageConverter(getActivity());
        volleyService = new VolleyService(resultCallback,getActivity());

        if (authentication.checkIfLogedIn() == true){

            param.put("user_id",authentication.getUserPreference().getString("id",null));
            getProductListUrl = "/get-other-product-list";
        }
        loadProducts();

        return view;
    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getProductListCallbackCode) {
                    productList.clear();
                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            productList.add(product);
                        }
                        Log.i("product_list",""+productList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    productAdapter.notifyDataSetChanged();
                }

                Log.i("product_data",""+response);
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("product_data error",""+error.getMessage());
                swipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void onRecyclerViewItemClick(int position) {

        Toast.makeText(getActivity(),"this is id"+position,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRefresh() {

        loadProducts();
    }

    private void loadProducts() {

        swipeRefreshLayout.setRefreshing(true);

        volleyService.makeApiRequest(getProductListCallbackCode,getProductListUrl,param);

    }
}
