package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ChatActivity;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.data.AllChatList;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import java.util.List;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    private Context mContext;
    private List<AllChatList> allChatLists;
    private ImageConverter imageConverter;
    private String base64String;
    boolean isCustomise = false;

    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_chat_list_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        AllChatList allChatList = allChatLists.get(position);
        holder.tvPartnerName.setText(allChatList.getPartnerName());
        holder.tvMessaageTime.setText(allChatList.getLastMessageTime());
        holder.tvLastMessage.setText(allChatList.getLastMessage());

        if (!allChatList.getPartnerImage().equals("null")) {

            Log.i("check_img", allChatList.getPartnerImage());
            holder.imvPartnerImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.abc_ab_share_pack_mtrl_alpha));
        }

        // loading album cover using Glide library
//        Glide.with(mContext).load().into(holder.shop_img);
    }

    @Override
    public int getItemCount() {
        return allChatLists.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView  tvPartnerName, tvLastMessage, tvMessaageTime;
        public ImageView imvPartnerImage;

        public MyViewHolder(View view) {
            super(view);
            imvPartnerImage = (ImageView) view.findViewById(R.id.imv_partner_image);
            tvLastMessage = (TextView) view.findViewById(R.id.tv_last_message);
            tvMessaageTime = (TextView)view.findViewById(R.id.tv_message_time);
            tvPartnerName = (TextView)view.findViewById(R.id.tv_partner_name);

            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {


            switch (view.getId()) {
                case R.id.img_shop:
                    AllChatList allChatList = allChatLists.get(this.getAdapterPosition());

                    Intent intent = new Intent(mContext, ChatActivity.class);
                    intent.putExtra("shop_id", allChatList.getShopId());
                    intent.putExtra("customer_id", allChatList.getCustomerId());

                    mContext.startActivity(intent);

                    break;

                case R.id.contact:

                    Toast.makeText(mContext, "this is contact " , Toast.LENGTH_LONG).show();
                    break;
            }

        }
    }

    public ChatListAdapter(Context mContext, List<AllChatList> allChatLists) {

        this.mContext = mContext;
        this.allChatLists = allChatLists;

    }

}
