package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener{

    private AppCompatButton btnSingUp;

    private EditText firstName, lastName, userName,
            phone, email;


    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int updateUserCallbackCode = 1;
    private String updateUserUrl = "/update-user";
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.sign_up));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));


        btnSingUp = (AppCompatButton)findViewById(R.id.btn_sign_up);
        btnSingUp.setOnClickListener(this);

        firstName = (EditText)findViewById(R.id.first_name);
        lastName = (EditText)findViewById(R.id.last_name);
        userName = (EditText)findViewById(R.id.user_name);
        phone = (EditText)findViewById(R.id.phone);
        email = (EditText)findViewById(R.id.email);

        authentication = new Authentication(getApplication());
        firstName.setText(authentication.getUserPreference().getString("first_name",""));
        lastName.setText(authentication.getUserPreference().getString("last_name",""));
        userName.setText(authentication.getUserPreference().getString("user_name",""));
        phone.setText(authentication.getUserPreference().getString("phone",""));
        email.setText(authentication.getUserPreference().getString("email",""));

        //sever
        initVolleyCallback();

        volleyService = new VolleyService(resultCallback,getApplication());
    }



    private void initVolleyCallback(){
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                try {
                    authentication.getUserEditor().putString("id",response.getString("id"));
                    authentication.getUserEditor().putString("first_name",response.getString("first_name"));
                    authentication.getUserEditor().putString("last_name",response.getString("last_name"));
                    authentication.getUserEditor().putString("user_name",response.getString("user_name"));
                    authentication.getUserEditor().putString("phone",response.getString("phone"));
                    authentication.getUserEditor().putString("email",response.getString("email"));
                    authentication.getUserEditor().putString("image",response.getString("image"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                authentication.commitChangeUser();

                Log.i("response",authentication.getUserPreference().getString("id",null)+","+
                        authentication.getUserPreference().getString("first_name",null)+","+
                        authentication.getUserPreference().getString("last_name",null)+","+
                        authentication.getUserPreference().getString("user_name",null)+","+
                        authentication.getUserPreference().getString("phone",null)+","+
                        authentication.getUserPreference().getString("email",null)+",");

                Log.i("response",response+"");

                startActivity(new Intent(getApplication(), UserDetailActivity.class));

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                Log.i("error",error+"");
                Toast.makeText(EditUserActivity.this, "this is error "+error, Toast.LENGTH_SHORT).show();

            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign_up:
                swipeRefreshLayout.setRefreshing(true);
                param.put("first_name",firstName.getText().toString());
                param.put("last_name",lastName.getText().toString());
                param.put("user_name",userName.getText().toString());
                param.put("phone",phone.getText().toString());
                param.put("email",email.getText().toString());
                param.put("id",authentication.getUserPreference().getString("id",null));

                volleyService.makeApiRequest(updateUserCallbackCode, updateUserUrl,param);
                //passInputLayout.setError("password miss match!");
                break;

        }
    }
}
