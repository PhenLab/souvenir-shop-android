package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/25/2018.
 */

public class AllChatList {

    private String lastMessage;
    private String partnerName;
    private String lastMessageTime;
    private String shopId;
    private String customerId;
    private String partnerImage;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPartnerImage() {
        return partnerImage;
    }

    public void setPartnerImage(String partnerImage) {
        this.partnerImage = partnerImage;
    }

    public String getShopId() {

        return shopId;

    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public AllChatList(String lastMessage, String partnerName,
                       String lastMessageTime, String shopId, String partnerImage, String customerId) {
        this.lastMessage = lastMessage;
        this.partnerName = partnerName;
        this.lastMessageTime = lastMessageTime;
        this.shopId = shopId;
        this.partnerImage = partnerImage;
        this.customerId = customerId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }
}
