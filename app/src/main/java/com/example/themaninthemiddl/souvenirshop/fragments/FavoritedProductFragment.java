package com.example.themaninthemiddl.souvenirshop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.favoriteProductAdapter;
import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritedProductFragment extends Fragment {


    private RecyclerView recyclerView;
    private favoriteProductAdapter favoritedProductAdapter;
    private List<Products> favoritedProductList;

    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getFavoritedProductListCallbackCode = 1;
    private String getFavoritedProductUrl = "/product/favorite/by-user-id";

    private ImageView addProduct, searchFilter;
    private ImageConverter imageConverter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorited_product, container, false);


        recyclerView = (RecyclerView)view.findViewById(R.id.favorited_product_recycler_view);

        favoritedProductList = new ArrayList<>();

        favoritedProductAdapter = new favoriteProductAdapter(getActivity(), favoritedProductList,true);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(favoritedProductAdapter);

        authentication = new Authentication(getActivity());

        param.put("user_id",authentication.getUserPreference().getString("id",null));
        param.put("type",getString(R.string.product_type));

        initVolleyCallback();
        imageConverter = new ImageConverter(getActivity());
        volleyService = new VolleyService(resultCallback,getActivity());
        volleyService.makeApiRequest(getFavoritedProductListCallbackCode, getFavoritedProductUrl,param);


        return view;

    }

    private void initVolleyCallback() {
        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONArray jsonArray = null;
                JSONObject jsonObject;
                if (callBackCode == getFavoritedProductListCallbackCode) {

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            favoritedProductList.add(product);
                        }
                        Log.i("product_list",""+ favoritedProductList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    favoritedProductAdapter.notifyDataSetChanged();
                }

                Log.i("favorite_product_data",""+response);
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {
                Log.i("favorite_pro_data_error",""+error.getMessage());

            }
        };
    }


}
