package com.example.themaninthemiddl.souvenirshop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.themaninthemiddl.souvenirshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment  {


    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        //progress bar
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        progressBar.setVisibility(View.VISIBLE);

        return view;
    }

}
