package com.example.themaninthemiddl.souvenirshop.activities;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.app.AlertDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.adapters.productAdapter;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.LocationService;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageConverter imageConverter;

    private String imageBase64String;

    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getProductByIDCallbackCode = 1;
    private String getProductByIDUrl = "/get-product-by-id";

    private int getProductCountByShopIDCallbackCode = 2;
    private String getProductCountByShopIDUrl = "/product/count-by-shop-id";
    private int getRelatedProductCallbackCode = 3;
    private String getRelatedProductUrl = "/get-product-list-by-category-id";
    private int getShopCallbackCode = 4;

    private String createFavoritedProductUrl = "/favorite/add";
    private int createFavoritedProductCallbackCode = 6;
    private String deleteFavoritedProductUrl = "/favorite/delete";
    private int deleteFavoritedProductCallbackCode = 7;

    private String getShopUrl = "/get-shop-by-id";

    private String deleteProductUrl = "/product/delete";
    private int deleteProductCallbackCode = 5;

    private CircleImageView shopImage;

    String shop_id = null;
    String shopName = null;

    private AppBarLayout appBarLayout;
    private ImageView ivImage, editProduct,imvDeletProduct, imvFavorite;
    RelativeLayout products, messages, openShop;
    private TextView tv_location, tvPrice,tvCategory, tvEmail, tvTotalInShop,tvQuantity,
            tvDescription, tvShopName, tvProductName, tvPostDate;

    private productAdapter productAdapter;
    private List<Products> relatedProductList = new ArrayList<>();

    CollapsingToolbarLayout toolbarLayout;
    private RecyclerView relateProductRecyclerView;
    AppCompatButton btnContact;
    boolean hasPhone = false,hasEmail = false, hasWebsite= false ,is_favorite = false;
    String Phone, Email, Website, product_name,Quantity, Price,category_id, Description, imageBase64;

    private LocationService locationService;
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.product_detail));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

                if (getIntent().getBooleanExtra(getString(R.string.login_to_get_permision),false) == true){
                    startActivity(new Intent(getApplication(),MainActivity.class));
                }
            }
        });

        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolbarLayout.setTitle(getString(R.string.product_detail));
//        toolbarLayout.setContentScrimColor(R.color.white);
//        toolbarLayout.setStatusBarScrimColor(R.color.white);


        tv_location = (TextView)findViewById(R.id.location);
        tvDescription = (TextView)findViewById(R.id.description);
        tvShopName = (TextView)findViewById(R.id.shop_name);
        tvProductName = (TextView)findViewById(R.id.product_name);
        tvPostDate = (TextView)findViewById(R.id.post_date);
        tvTotalInShop = (TextView)findViewById(R.id.product_quantity);
        tvCategory = (TextView)findViewById(R.id.category);
        tvPrice = (TextView)findViewById(R.id.product_price);

        ivImage = (ImageView) findViewById(R.id.image);
        imvFavorite = (ImageView) findViewById(R.id.imvFavorite);

        tvQuantity = (TextView) findViewById(R.id.quantity);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        shopImage = (CircleImageView) findViewById(R.id.shop_image);
        editProduct = (ImageView) findViewById(R.id.edit_product);
        imvDeletProduct = (ImageView) findViewById(R.id.delete_product);

        openShop = (RelativeLayout) findViewById(R.id.open_shop);
        btnContact = (AppCompatButton) findViewById(R.id.contact);

        btnContact.setOnClickListener(this);
        imvFavorite.setOnClickListener(this);
        openShop.setOnClickListener(this);
        editProduct.setOnClickListener(this);
        imvDeletProduct.setOnClickListener(this);

        authentication = new Authentication(this);

        locationService = new LocationService(getApplication());

        relateProductRecyclerView = (RecyclerView)findViewById(R.id.releted_product_recycler);

        productAdapter = new productAdapter(getApplication(), relatedProductList,false);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        relateProductRecyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        relateProductRecyclerView.setItemAnimator(new DefaultItemAnimator());

        relateProductRecyclerView.setAdapter(productAdapter);



        imageConverter = new ImageConverter(this);
        //api
        initVolleyCallback();
        volleyService = new VolleyService(resultCallback,this);

        param.put("id",getIntent().getStringExtra("product_id"));
        editProduct.setVisibility(View.VISIBLE);

        loadProduct();


    }

    private void loadProduct() {

        volleyService.makeApiRequest(getProductByIDCallbackCode,getProductByIDUrl,param);

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {

                JSONObject jsonObject;
                JSONArray jsonArray = null;

                if (callBackCode == getProductByIDCallbackCode){


                    try {

                        jsonArray = response.getJSONArray("data");
                        Log.i("product_detail",""+response);
                        jsonObject = jsonArray.getJSONObject(0);



                        if(authentication.checkIfOwner(jsonObject.getString("created_by")) == true){
                            Log.i("is_own","true");
                            editProduct.setVisibility(View.VISIBLE);
                            imvDeletProduct.setVisibility(View.VISIBLE);

                        }
                        else {
                            if(!(jsonObject.getString("is_favorite")).equals("0")){
                                is_favorite = true;

                                imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
                            }
                            Log.i("check_fav",(jsonObject.getString("is_favorite")));
                            editProduct.setVisibility(View.GONE);
                            imvDeletProduct.setVisibility(View.GONE);
                            imvFavorite.setVisibility(View.VISIBLE);

                        }

                        String imgBase64 = jsonObject.getString("image");
                        shop_id = jsonObject.getString("shop_id");

                        param.put("category_id",jsonObject.getString("category_id"));

                        param.put("shop_id",shop_id);


                        String shopImgBase64 = jsonObject.getString("shop_image");

                        if(!(shopImgBase64).equals("null")) {
                            shopImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(shopImgBase64));
                        }
                        if (!(imgBase64).equals("null")  ) {
                            Bitmap bitmap = imageConverter.convertFromBase64ToBitmap(jsonObject.getString("image"));

                            int containerHeight = (bitmap.getHeight()-bitmap.getWidth())+386;
                            float density = getApplication().getResources().getDisplayMetrics().density;
                            int px = containerHeight * (int)density;

                            ivImage.setImageBitmap(imageConverter.convertFromBase64ToBitmap(imgBase64));
                            appBarLayout.setLayoutParams(new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT,px));
                        }

                        product_name = jsonObject.getString("name");
                        Description = jsonObject.getString("description");
                        Quantity = jsonObject.getString("quantity");
                        Price = jsonObject.getString("price");
                        category_id = jsonObject.getString("category_id");
                        imageBase64 = jsonObject.getString("image");

                        tvProductName.setText(jsonObject.getString("name"));
//                        toolbarLayout.setTitle(jsonObject.getString("name"));

                        String lat = jsonObject.getString("lat");
                        String lng = jsonObject.getString("lng");



                        if (!(lat).equals("null")){

                            tv_location.setText(locationService.getFullLocation(Double.valueOf(lat),Double.valueOf(lng)));
                        }

                        tvProductName.setText(product_name);
                        tvPostDate.setText(jsonObject.getString("updated_at"));
                        tvDescription.setText(jsonObject.getString("description"));
                        tvShopName.setText(jsonObject.getString("shop_name"));
                        shopName= jsonObject.getString("shop_name");

                        tvQuantity.setText(jsonObject.getString("quantity"));
                        tvCategory.setText(jsonObject.getString("category_name"));
                        tvPrice.setText(getString(R.string.currency)+jsonObject.getString("price"));

                        volleyService.makeApiRequest(getProductCountByShopIDCallbackCode,getProductCountByShopIDUrl,param);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                else if(callBackCode == getRelatedProductCallbackCode){

                    boolean isError = false;
                    try {
                        jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            jsonObject = jsonArray.getJSONObject(i);
                            Products product = new Products(jsonObject.getString("id"),jsonObject.getString("image"),
                                    jsonObject.getString("name"), jsonObject.getString("price"),
                                    jsonObject.getString("category_name"), jsonObject.getString("shop_name"));
                            relatedProductList.add(product);
                        }
                        Log.i("product_list",""+relatedProductList);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("rupp", "Error converting documents");
                        isError = true;
                    }
                    productAdapter.notifyDataSetChanged();
                    Log.i("re_pro",""+response);
                }
                else if (callBackCode == getShopCallbackCode) {

                    Log.i("contact",response+"");
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ProductDetailActivity.this, android.R.layout.select_dialog_singlechoice);

                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(ProductDetailActivity.this);

                    try {

                        jsonObject = response.getJSONObject("data");
                        Phone = jsonObject.getString("phone");
                        Email = jsonObject.getString("email");
                        Website = jsonObject.getString("website");

                        if (Phone != null || Phone != "null") {
                            hasPhone = true;
                            arrayAdapter.add(Phone);
                        }
                        if (Website != null || Website != "null") {
                            hasWebsite = true;
                            arrayAdapter.add(Website);
                        }
                        if (Email != null || Email != "null") {
                            arrayAdapter.add(Email);
                            hasEmail = true;
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    builderSingle.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String strName = arrayAdapter.getItem(which);

                            if (which == 0) {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Phone ));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
//                              launchCameraClick();
                            }
                            if (which == 1)
                            {
                                if (hasWebsite == false) {

//                              openGalleryClick();
                                }
                                else {
                                    try {
                                        Uri webpage = Uri.parse(Website);
                                        Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
                                        startActivity(myIntent);
                                    } catch (ActivityNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            else{
                                
                                Toast.makeText(ProductDetailActivity.this, "website", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });

                    builderSingle.show();

                }
                else if(callBackCode == createFavoritedProductCallbackCode){
                    is_favorite = true;
                    imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite));
                }


                else if (callBackCode == getProductCountByShopIDCallbackCode){


                    try {
                        tvTotalInShop.setText("+"+response.getString("data"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    volleyService.makeApiRequest(getRelatedProductCallbackCode,getRelatedProductUrl,param);

                }
                else if (callBackCode == deleteProductCallbackCode){
                    Intent intent = new Intent(getApplication(), ShopProductActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("shop_id",shop_id);
                    intent.putExtra("shop_name",shopName);
                    startActivity(intent);
                }
                else if (callBackCode == deleteFavoritedProductCallbackCode){

                    Log.i("favorite_data",""+response);

                    imvFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_favorite));
                    is_favorite = false;
                }

            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Log.i("get_pr_error","error");
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.edit_product:
                Intent intents = new Intent(getApplication(),EditProductActivity.class);
                intents.putExtra("product_id",getIntent().getStringExtra("product_id"));
                intents.putExtra("category_id",category_id);
                intents.putExtra("name",product_name);

                intents.putExtra("price",Price);
                intents.putExtra("quantity",Quantity);
                intents.putExtra("image",imageBase64);
                intents.putExtra("description",Description);

                startActivity(intents);
                break;

            case R.id.open_shop:

                Intent intent = new Intent(getApplication(),ShopDetailActivity.class);
                intent.putExtra("SHOP_ID",shop_id);
                startActivity(intent);
                break;

            case R.id.contact:


                volleyService.makeApiRequest(getShopCallbackCode,getShopUrl,param);
                break;

            case R.id.delete_product:
                alertDelete();
                break;

            case R.id.imvFavorite:

                if (is_favorite == true){

                    alertRemoveFavoriteMessage();

                }
                else {
                    if (authentication.checkIfLogedIn() == true) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", getIntent().getStringExtra("product_id"));
                        param.put("type", getString(R.string.product_type));

                        volleyService.makeApiRequest(createFavoritedProductCallbackCode, createFavoritedProductUrl, param);
                    } else {

                        Intent intent1 = new Intent(getApplication(), SignInActivity.class);
                        intent1.putExtra("is_favorite", true);
                        intent1.putExtra("type", getString(R.string.product_type));
                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent1.putExtra("item_id", getIntent().getStringExtra("product_id"));
                        startActivity(intent1);
                        finish();

                    }
                }
                break;


        }
    }


    public void alertDelete(){

        new android.support.v7.app.AlertDialog.Builder(this)

                .setMessage(getString(R.string.confirm_delete))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        param.put("user_id",authentication.getUserPreference().getString("id",null));
                        volleyService.makeApiRequest(deleteProductCallbackCode,deleteProductUrl,param);
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    public void alertAddFavoriteMessage(String message){

        new android.support.v7.app.AlertDialog.Builder(this)

                .setMessage(getString(R.string.confirm_add_favorite))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(getApplication(), getString(R.string.confirm_add_favorite), Toast.LENGTH_SHORT).show();
                    }});

    }

    public void alertRemoveFavoriteMessage(){

        new android.support.v7.app.AlertDialog.Builder(this)

                .setMessage(getString(R.string.confirm_remove_favorite_product))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", getIntent().getStringExtra("product_id"));
                        param.put("type", getString(R.string.product_type));

                        Log.i("post_fav",""+param);
                        volleyService.makeApiRequest(deleteFavoritedProductCallbackCode, deleteFavoritedProductUrl, param);

                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }


}
