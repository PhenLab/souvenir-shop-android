package com.example.themaninthemiddl.souvenirshop;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;

import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.activities.SignInActivity;
import com.example.themaninthemiddl.souvenirshop.activities.UserDetailActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;
import com.example.themaninthemiddl.souvenirshop.fragments.AboutUsFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.FavoriteFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.HelpFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.HomeFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.MessageFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.NotificationFragment;
import com.example.themaninthemiddl.souvenirshop.fragments.ShopFragment;
import com.example.themaninthemiddl.souvenirshop.notifications.app.Config;
import com.example.themaninthemiddl.souvenirshop.notifications.util.NotificationUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{



    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView imgProfile;
    private ImageView imgSearchFilter;

    //data in share preference
    private Authentication authentication;
    TextView toolbarTitle;
    //data in share preference
    int oldPosition = 0;

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;
    private int getUserCallbackCode = 2;
    private String getUserByIDUrl = "/get-user-by-id";
    Map<String,String> param = new HashMap<String, String>();
    private static final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    ImageConverter imageConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setRotationX(180);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        toolbarTitle = (TextView) findViewById(R.id.main_toolbar_title);
        imgSearchFilter = (ImageView) findViewById(R.id.search_filter);

        setupViewPager(viewPager);
        int pageIndex = getIntent().getIntExtra("page_index",-1);
        if(pageIndex != -1) {
            tabLayout.setScrollPosition(pageIndex, 0f, true);
            viewPager.setCurrentItem(pageIndex);

        }
        tabLayout.setupWithViewPager(viewPager);


        setupTabIcons();

        initVolleyCallback();
        authentication = new Authentication(this);
        imageConverter = new ImageConverter(this);
        volleyService = new VolleyService(resultCallback,this);
        if (authentication.checkIfLogedIn() == true) {

            param.put("user_id",authentication.getUserPreference().getString("id",null));
            volleyService.makeApiRequest(getUserCallbackCode,getUserByIDUrl,param);
        }

        imgProfile = (ImageView)findViewById(R.id.img_profile);
        imgProfile.setOnClickListener(this);
        authentication = new Authentication(getApplication());
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override

            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                if (position == 0){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
                    imgSearchFilter.setVisibility(View.VISIBLE);
//                    changeTabNormalStatus(oldPosition);
//                    oldPosition = position;
//                    changeTabStatus(R.string.home,position+1,R.drawable.ic_tab_home,R.color.colorPrimary);
                    toolbarTitle.setText(getString(R.string.home));
                }
                else if(position == 1){

                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
//                    changeTabNormalStatus(oldPosition);
//                    oldPosition = position;
//                    changeTabStatus(R.string.shop,position,R.drawable.ic_tab_shop,R.color.colorPrimary);
                    imgSearchFilter.setVisibility(View.GONE);
                    toolbarTitle.setText(getString(R.string.shop));
                }
                else if(position == 2){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
//                    changeTabNormalStatus(oldPosition);
//                    oldPosition = position;
//                    changeTabStatus(R.string.message,position,R.drawable.ic_tab_message,R.color.colorPrimary);

                    imgSearchFilter.setVisibility(View.GONE);
                    toolbarTitle.setText(getString(R.string.message));
                }
                else if(position == 3){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
//                    changeTabNormalStatus(oldPosition);
//                    oldPosition = position;
//                    changeTabStatus(R.string.favorite,position,R.drawable.ic_tab_favorite,R.color.colorPrimary);
                    toolbarTitle.setText(getString(R.string.favorite));
                    imgSearchFilter.setVisibility(View.GONE);

                }
                else if(position == 4){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
//                    changeTabNormalStatus(oldPosition);
//                    oldPosition = position;
//                    changeTabStatus(R.string.notification,position,R.drawable.ic_tab_notifications,R.color.colorPrimary);
                    toolbarTitle.setText(getString(R.string.notification));
                    imgSearchFilter.setVisibility(View.GONE);

                }
                else if(position == 5){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
                    toolbarTitle.setText(getString(R.string.about_us));
                    imgSearchFilter.setVisibility(View.GONE);

                }
                else if(position == 6){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
                    toolbarTitle.setText(getString(R.string.help));
                    imgSearchFilter.setVisibility(View.GONE);

                }

                else if(position == 7){
                    tabLayout.setScrollPosition(position, 0f, true);
                    viewPager.setCurrentItem(position);
                    toolbarTitle.setText(getString(R.string.notification));
                    imgSearchFilter.setVisibility(View.GONE);

                }



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();


        Query getChatRef = mDatabase.child("chats")
                .orderByChild("myType_myId_partnerType_partnerId").equalTo("2_1_1_1");

        getChatRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                Log.i("check_data","s"+s+","+"dataSnapshot"+dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                Log.i("check_data_change","change");

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        getChatRef.addChildEventListener(childEventListener);
        Log.i("read_data",""+mDatabase.child("chats").child("id").toString());

        //notifications

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplication(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    Log.i("message",message);
                }
            }
        };

        displayFirebaseRegId();

    }

    private void initVolleyCallback() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {
                JSONObject jsonObject;
                Log.i("get_image",""+response);
                try {
                    jsonObject = response.getJSONObject("data");

                    String imageBase64, image;
                    Bitmap imgBitmap;
                    imageBase64 = jsonObject.getString("image");
                    if (!(imageBase64.equals("null"))) {
                        imgBitmap = imageConverter.convertFromBase64ToBitmap(imageBase64);
                        imgProfile.setImageBitmap(imgBitmap);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Log.i("get_user_error","getu"+error);
            }
        };
    }


//    Drawable drawable = R.drawable.ic_tab_home;

    private void setupTabIcons() {


        android.widget.TextView tabHome = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText("Home");
        Drawable mDrawable = getApplication().getResources().getDrawable(R.drawable.ic_tab_home);
        mDrawable.setColorFilter(new
                PorterDuffColorFilter(getResources().getColor(R.color.colorPrimary),PorterDuff.Mode.MULTIPLY));
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.ic_tab_home , 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabHome);

        mDrawable.setColorFilter(new
                PorterDuffColorFilter(getResources().getColor(R.color.grey4),PorterDuff.Mode.MULTIPLY));
        android.widget.TextView tabShop = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabShop.setText("Shops");
        tabShop.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_shop, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabShop);

        android.widget.TextView tabMessage = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabMessage.setText("Message");
        tabMessage.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_message, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabMessage);

        android.widget.TextView tabFavorite = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFavorite.setText("Favorites");
        tabFavorite.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_favorite, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFavorite);


        android.widget.TextView tabNontification = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabNontification.setText("Nontifications");
        tabNontification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_notifications, 0, 0);
        tabLayout.getTabAt(4).setCustomView(tabNontification);

        android.widget.TextView tabHelp = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHelp.setText("About Us");
        tabHelp.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_about_us, 0, 0);
        tabLayout.getTabAt(5).setCustomView(tabHelp);

        android.widget.TextView tabFour = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Helps");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_favorite, 0, 0);
        tabLayout.getTabAt(6).setCustomView(tabFour);
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "ONES");
        adapter.addFragment(new ShopFragment(), "TWOS");
        adapter.addFragment(new MessageFragment(), "THREES");
        adapter.addFragment(new FavoriteFragment(), "FOURS");
        adapter.addFragment(new NotificationFragment(), "FOURS");
        adapter.addFragment(new AboutUsFragment(), "FOURS");
        adapter.addFragment(new HelpFragment(), "FOURS");

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter{


        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(android.support.v4.app.FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

//  click listener

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            //user accounts
            case R.id.img_profile:

                Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                if(authentication.checkIfLogedIn() == true){

                    startActivity(new Intent(getApplication(),UserDetailActivity.class));

                }else {


                    startActivity(new Intent(getApplication(),SignInActivity.class));
                    finish();
                }
                break;
        }

    }

    public void changeTabStatus(int tabTitle, int tabPosition, int imgIcon ,int titleColor){

        android.widget.TextView tabHome = (android.widget.TextView) android.view.LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabHome.setText(getString(tabTitle));
        Drawable mDrawable = getApplication().getResources().getDrawable(imgIcon);
        mDrawable.setColorFilter(new
                PorterDuffColorFilter(getResources().getColor(titleColor),PorterDuff.Mode.MULTIPLY));
        tabHome.setCompoundDrawablesWithIntrinsicBounds(0,imgIcon, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabHome);

    }



    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);
    }

    @Override
    public void onResume() {

        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(getApplication()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(getApplication()).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplication());
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getApplication()).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
