package com.example.themaninthemiddl.souvenirshop.data;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by The Man In The Middl on 5/4/2018.
 */

public class Authentication {

    private static Context context;
    private static SharedPreferences.Editor editor;

    private static SharedPreferences preference;
    public  Authentication(Context context) {
        this.context = context;
        preference = context.getSharedPreferences("user_data", MODE_PRIVATE);
        this.editor = preference.edit();

    }

    public SharedPreferences getUserPreference(){

        return preference;
    }
    public SharedPreferences.Editor getUserEditor(){

        return editor;
    }



    public void commitChangeUser(){
        editor.commit();
    }

    public static boolean checkIfLogedIn(){
        boolean Default = false;
        String username = preference.getString("id", null);

        if(username != null){
            Default = true;
        }
        return Default;

    }

    public static boolean checkIfOwner(String userId){
        boolean Default = false;
        String user_id = preference.getString("id", null);
        if (user_id !=null) {
            if (user_id.equals(userId)) {
                Default = true;
            }
        }
        return Default;

    }


    public static void logOut(){

        preference.edit().clear().commit();

    }
}
