package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ProductDetailActivity;
import com.example.themaninthemiddl.souvenirshop.api.RequestResult;
import com.example.themaninthemiddl.souvenirshop.api.VolleyService;
import com.example.themaninthemiddl.souvenirshop.data.Authentication;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class favoriteProductAdapter extends RecyclerView.Adapter<favoriteProductAdapter.MyViewHolder>  {

    private Context mContext;
    private List<Products> productList;
    private ImageConverter imageConverter;
    private boolean isCustomizedProductCard;


    //data in share preference
    private String userData = "user_data";
    private Authentication authentication;
    //data in share preference


    //server
    Map<String,String> param = new HashMap<String, String>();

    //server
    RequestResult resultCallback = null;
    VolleyService volleyService;

    private String deleteFavoritedProductUrl = "/favorite/delete";
    private int deleteFavoritedProductCallbackCode = 10;

    private productAdapter productAdapter;
    private List<Products> relatedProductList = new ArrayList<>();

    CollapsingToolbarLayout toolbarLayout;
    private RecyclerView relateProductRecyclerView;
    AppCompatButton btnContact;
    String product_id = null;
    int position = -1;


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        imageConverter = new ImageConverter(mContext);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_product_card, parent, false);


        initVolleyService();
        authentication = new Authentication(mContext);
        volleyService = new VolleyService(resultCallback , mContext);


        if (isCustomizedProductCard == true) {
            itemView.setLayoutParams(new TableLayout.LayoutParams(0, 800, 1f));
        }
        return new MyViewHolder(itemView);



    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Products products = productList.get(position);
        holder.name.setText(products.getName());
        holder.price.setText("$"+products.getPrice());
        holder.category.setText(products.getCategory());
        holder.shop_name.setText(products.getShop_name());
        if (!(products.getProduct_img()).equals("null")) {
            holder.product_img.setImageBitmap(imageConverter.convertFromBase64ToBitmap(products.getProduct_img()));
        }
        // loading album cover using Glide library
//        Glide.with(mContext).load(imageConverter.convertFromBase64ToUri(products.getProduct_img())).into(holder.product_img);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name, category, shop_name, price;
        public ImageView product_img;
        public RelativeLayout tvDelete;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.product_name);
            category = (TextView) view.findViewById(R.id.category);
            price = (TextView) view.findViewById(R.id.price);
            product_img = (ImageView) view.findViewById(R.id.img_product);
            tvDelete = (RelativeLayout) view.findViewById(R.id.delete_favorite_pro);
            shop_name = (TextView) view.findViewById(R.id.shop_name);
            product_img.setOnClickListener(this);
            tvDelete.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            position = this.getAdapterPosition();
            Products product = productList.get(position);
            product_id = product.getProduct_id();
            Intent intent;
            switch (view.getId()) {
                case R.id.img_product:
                    Toast.makeText(mContext, "open pro", Toast.LENGTH_SHORT).show();

                    intent = new Intent(mContext, ProductDetailActivity.class);
                    intent.putExtra("product_id", product.getProduct_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                    break;

                case R.id.delete_favorite_pro:
                    alertRemoveFavoriteMessage();
                    Toast.makeText(mContext, "delete fav"+position, Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    public favoriteProductAdapter(Context mContext, List<Products> productList, boolean isCustomizedProductCard) {

        this.mContext = mContext;
        this.productList = productList;
        this.isCustomizedProductCard = isCustomizedProductCard;

    }

    public void alertRemoveFavoriteMessage(){

        new android.support.v7.app.AlertDialog.Builder(mContext)

                .setMessage(mContext.getString(R.string.confirm_remove_favorite_product))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                        param.put("user_id", authentication.getUserPreference().getString("id", null));
                        param.put("item_id", product_id);
                        param.put("type", mContext.getString(R.string.product_type));

                        Log.i("post_fav",""+param);
                        volleyService.makeApiRequest(deleteFavoritedProductCallbackCode, deleteFavoritedProductUrl, param);


                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    public void initVolleyService() {

        resultCallback = new RequestResult() {
            @Override
            public void notifySuccess(int callBackCode, JSONObject response) {
                if (callBackCode == deleteFavoritedProductCallbackCode){
                    productList.remove(position);
                    Log.i("check_favorite_data",""+productList);
                    notifyDataSetChanged();

                }
            }

            @Override
            public void notifyError(int callBackCode, VolleyError error) {

                Toast.makeText(mContext, "remove error", Toast.LENGTH_SHORT).show();
            }
        };
    }

}
