package com.example.themaninthemiddl.souvenirshop.data;

/**
 * Created by The Man In The Middl on 5/25/2018.
 */

public class ChatMessages {

    private String messageText;
    private String messageSender;
    private String messageTime;
    private String myID;
    private String myType;
    private String senderImage;

    public String getMyType() {
        return myType;
    }

    public void setMyType(String myType) {
        this.myType = myType;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getMyID() {

        return myID;

    }

    public void setMyID(String myID) {
        this.myID = myID;
    }

    public ChatMessages(String messageText, String messageSender,
                        String messageTime, String myID, String senderImage,String myType) {
        this.messageText = messageText;
        this.messageSender = messageSender;
        this.messageTime = messageTime;
        this.myID = myID;
        this.senderImage = senderImage;
        this.myType = myType;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public void setMessageSender(String messageSender) {
        this.messageSender = messageSender;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }
}
