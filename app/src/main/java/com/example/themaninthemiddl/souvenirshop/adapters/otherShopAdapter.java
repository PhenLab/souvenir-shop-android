package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ShopDetailActivity;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Shops;

import java.util.List;

/**
 * Created by The Man In The Middl on 5/5/2018.
 */

public class otherShopAdapter extends RecyclerView.Adapter<otherShopAdapter.MyViewHolder>  {

    private Context mContext;
    private List<Shops> shopList;
    private ImageConverter imageConverter;
    private String base64String;

    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_card, parent, false);
        itemView.setLayoutParams(new TableLayout.LayoutParams(0, 700,1f));

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(otherShopAdapter.MyViewHolder holder, int position) {

        Shops shops = shopList.get(position);
        holder.product_number.setText("+"+shops.getProduct_number());
        holder.shop_name.setText(shops.getShop_name());
        if (!shops.getShop_img().equals("null")) {
            Log.i("check_img",shops.getShop_img());
            holder.shop_img.setImageBitmap(imageConverter.convertFromBase64ToBitmap(shops.getShop_img()));
        }

        // loading album cover using Glide library
//        Glide.with(mContext).load().into(holder.shop_img);
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView shop_name, product_number;
        public ImageView shop_img,favorite;

        public MyViewHolder(View view) {
            super(view);
//            favorite = (ImageView) view.findViewById(R.id.favorite);
            shop_img = (ImageView) view.findViewById(R.id.img_shop);
            shop_name = (TextView) view.findViewById(R.id.shop_name);
            product_number = (TextView)view.findViewById(R.id.product_number);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            Shops shops = shopList.get(this.getAdapterPosition());

            Intent intent = new Intent(mContext, ShopDetailActivity.class);
            intent.putExtra("SHOP_ID", shops.getId());
            mContext.startActivity(intent);


        }
    }

    public otherShopAdapter(Context mContext, List<Shops> shopList) {

        this.mContext = mContext;
        this.shopList = shopList;

    }

}
