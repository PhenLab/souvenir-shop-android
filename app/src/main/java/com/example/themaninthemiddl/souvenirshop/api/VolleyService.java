package com.example.themaninthemiddl.souvenirshop.api;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by lotphen on 12/29/17.
 */

public class   VolleyService {


    public static String baseUrl = "http://192.168.0.108:8000";
//    public static String baseUrl = "https://unsurpassed-saving.000webhostapp.com/khmer_souvenir/public/index.php";


    RequestResult mResultCallback = null;
    Context mContext;

    public VolleyService(RequestResult resultCallback, Context context){

        mResultCallback = resultCallback;
        mContext = context;

    }


    public void makeApiRequest(final int callBackCode , String url, Map<String,String> param){

            RequestQueue queue = Volley.newRequestQueue(mContext);

            JsonObjectRequest jsonObj = new JsonObjectRequest(baseUrl+url,new JSONObject(param), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    if(mResultCallback != null) {

                        mResultCallback.notifySuccess(callBackCode, response);
//                        Toast.makeText(mContext,"request ok "+response, Toast.LENGTH_LONG).show();


                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if(mResultCallback != null) {
//                        Toast.makeText(mContext,"request error "+error.getMessage(),Toast.LENGTH_LONG).show();

                        mResultCallback.notifyError(callBackCode, error);
//                        Log.d("server_error",error.getMessage());

                    }
                }
            });

            queue.add(jsonObj);


    }

//    public void getDataVolley(final String requestType, String url){
//        try {
//            RequestQueue queue = Volley.newRequestQueue(mContext);
//
//            JsonObjectRequest jsonObj = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
//                @Override
//                public void onResponse(JSONObject response) {
//                    if(mResultCallback != null)
//                        mResultCallback.notifySuccess(requestType, response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    if(mResultCallback != null)
//                        mResultCallback.notifyError(requestType, error);
//                }
//            });
//
//            queue.add(jsonObj);
//
//        }catch(Exception e){
//
//        }
//    }

}
