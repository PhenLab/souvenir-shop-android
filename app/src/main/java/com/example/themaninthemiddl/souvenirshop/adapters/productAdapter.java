package com.example.themaninthemiddl.souvenirshop.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.themaninthemiddl.souvenirshop.MainActivity;
import com.example.themaninthemiddl.souvenirshop.R;
import com.example.themaninthemiddl.souvenirshop.activities.ProductDetailActivity;
import com.example.themaninthemiddl.souvenirshop.data.ImageConverter;
import com.example.themaninthemiddl.souvenirshop.data.Products;

import java.util.List;

/**
 * Created by The Man In The Middl on 4/29/2018.
 */

public class productAdapter extends RecyclerView.Adapter<productAdapter.MyViewHolder> implements View.OnClickListener {

    private Context mContext;
    private List<Products> productList;
    private ImageConverter imageConverter;
    private boolean isCustomizedProductCard;

    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        imageConverter = new ImageConverter(mContext);
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_card, parent, false);


        itemView.setOnClickListener(this);
        if (isCustomizedProductCard == true) {
            itemView.setLayoutParams(new TableLayout.LayoutParams(0, 700, 1f));
        }
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Products products = productList.get(position);
        holder.name.setText(products.getName());
        holder.price.setText("$"+products.getPrice());
        holder.category.setText(products.getCategory());
        holder.shop_name.setText(products.getShop_name());
        if (!(products.getProduct_img()).equals("null")) {
            holder.product_img.setImageBitmap(imageConverter.convertFromBase64ToBitmap(products.getProduct_img()));
        }
        // loading album cover using Glide library
//        Glide.with(mContext).load(imageConverter.convertFromBase64ToUri(products.getProduct_img())).into(holder.product_img);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public void onClick(View view) {


    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name, category, shop_name, price;
        public ImageView product_img;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.product_name);
            category = (TextView) view.findViewById(R.id.category);
            price = (TextView) view.findViewById(R.id.price);
            product_img = (ImageView) view.findViewById(R.id.img_product);
            shop_name = (TextView) view.findViewById(R.id.shop_name);
            view.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            Products product = productList.get(this.getAdapterPosition());
            Intent intent = new Intent(mContext,ProductDetailActivity.class);
            intent.putExtra("product_id",product.getProduct_id());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);


        }
    }

    public productAdapter(Context mContext, List<Products> productList, boolean isCustomizedProductCard) {

        this.mContext = mContext;
        this.productList = productList;
        this.isCustomizedProductCard = isCustomizedProductCard;

    }

}
