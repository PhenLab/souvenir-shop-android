package com.example.themaninthemiddl.souvenirshop.api;

/**
 * Created by lotphen on 12/29/17.
 */

public class CallbackCode {


    public static String ip = "http://192.168.1.168:8000";

    public static String Singin = "/login";
    private static String SingUp = "/register";

    private final static int province_callBack_code = 1;
    private final static int district_callBack_code = 2;
    private final static int commune_callBack_code = 3;
    private final static int village_callBack_code = 4;
    private final static int user_callBack_code = 5;



    private static String provinceList = "/get-province-list";


    public static int getProvinceCallBackCode(){

        return province_callBack_code;
    }

    public static int getDistrictCallBackCode(){

        return district_callBack_code;
    }

    public static int getCommuneCallBackCode(){

        return commune_callBack_code;
    }

    public static int getVillageCallBackCode(){

        return village_callBack_code;
    }
    public static int getUserCallBackCode(){

        return user_callBack_code;
    }

}
